<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_psb extends CI_Model
{
	var $table = 'tb_siswa_lengkap';
	var $column_order = array(null, 'ID_SISWA', 'ID_TRANSPORTASI', 'ID_AGAMA', 'JRK_SKLH', 'ID_ADMIN', 'ID_SKLH', 'NAMA_SISWA', 'NISN', 'NO_INDUK', 'TGL_LAHIR', 'TMP_LAHIR', 'JK_SISWA', 'ALAMAT_SISWA', 'ID_TINGGAL', 'JML_SDR', 'ANAK_KE', 'JENIS_SDR', 'KELAS_PARALEL', 'NOMOR_ABSEN', 'TAHUN_ANGKATAN', 'NO_KK', 'NO_NIK', 'ID_STTS_SISWA', 'ID_DESA', 'STATUS', 'FOTO', 'TANGGAL_MASUK', 'ID_PNDDKN_IBU', 'ID_PKRJN_IBU', 'ID_AGAMA_IBU', 'NAMA_ORTU_IBU', 'NIK_ORTU_IBU', 'TMP_LAHIR_ORTU_IBU', 'TGL_LAHIR_ORTU_IBU', 'ID_PENGHASILAN_IBU', 'ID_PNDDKN_AYAH', 'ID_PKRJN_AYAH', 'ID_AGAMA_AYAH', 'NAMA_ORTU_AYAH', 'NIK_ORTU_AYAH', 'TMP_LAHIR_ORTU_AYAH', 'TGL_LAHIR_ORTU_AYAH', 'ID_PENGHASILAN_AYAH', 'NO_HP_AYAH', 'NO_SERI_SKHUN', 'NO_SERI_IJAZAH', 'NO_PSRT_UN', 'TOTAL_NILAI_UN', 'TGL_KELULUSAN', 'BERKAS_PHOTO', 'BERKAS_AKTE', 'BERKAS_KK', 'BERKAS_IJAZAH', 'BERKAS_SKHUN'); //set column field database for datatable orderable
	var $column_search = array('ID_SISWA', 'ID_TRANSPORTASI', 'ID_AGAMA', 'JRK_SKLH', 'ID_ADMIN', 'ID_SKLH', 'NAMA_SISWA', 'NISN', 'NO_INDUK', 'TGL_LAHIR', 'TMP_LAHIR', 'JK_SISWA', 'ALAMAT_SISWA', 'ID_TINGGAL', 'JML_SDR', 'ANAK_KE', 'JENIS_SDR', 'KELAS_PARALEL', 'NOMOR_ABSEN', 'TAHUN_ANGKATAN', 'NO_KK', 'NO_NIK', 'ID_STTS_SISWA', 'ID_DESA', 'STATUS', 'FOTO', 'TANGGAL_MASUK', 'ID_PNDDKN_IBU', 'ID_PKRJN_IBU', 'ID_AGAMA_IBU', 'NAMA_ORTU_IBU', 'NIK_ORTU_IBU', 'TMP_LAHIR_ORTU_IBU', 'TGL_LAHIR_ORTU_IBU', 'ID_PENGHASILAN_IBU', 'ID_PNDDKN_AYAH', 'ID_PKRJN_AYAH', 'ID_AGAMA_AYAH', 'NAMA_ORTU_AYAH', 'NIK_ORTU_AYAH', 'TMP_LAHIR_ORTU_AYAH', 'TGL_LAHIR_ORTU_AYAH', 'ID_PENGHASILAN_AYAH', 'NO_HP_AYAH', 'NO_SERI_SKHUN', 'NO_SERI_IJAZAH', 'NO_PSRT_UN', 'TOTAL_NILAI_UN', 'TGL_KELULUSAN', 'BERKAS_PHOTO', 'BERKAS_AKTE', 'BERKAS_KK', 'BERKAS_IJAZAH', 'BERKAS_SKHUN'); //set column field database for datatable searchable 
	var $order = array('ID_SISWA' => 'DESC'); // default order

	public function getWilayah($id)
	{
		$this->db->from('wilayah');
		$this->db->where('ID_DESA',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function detil($id)
	{
		
		return $query = $this->db->get_where('v_siswa_lengkap',array('ID_SISWA' =>$id));
	}
	public function ortu($id)
	{
		return $query = $this->db->get_where('v_ortupsb2',array('ID_SISWA' =>$id));
	}

	// load datatabel 
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from('v_siswa_lengkap');

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function simpan_siswa($data){
		$this->db->insert('tb_siswa_lengkap',$data);
    	return $this->db->insert_id();
	}

	public function simpan_siswa_ijazah($dataIjazah){
		$this->db->insert('ijazah',$dataIjazah);
		return $this->db->insert_id();
	}

	public function hapusData($id){
		$this->db->where('ID_SISWA',$id);
		$this->db->delete('tb_siswa');
		$this->db->query('DELETE FROM ijazah WHERE ID_SISWA ='.$id);
	}

	public function get_by_id($id)
	{
		$this->db->from('v_siswa_lengkap');
		$this->db->where('ID_SISWA',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function select_id(){
		$query = $this->db->query("SELECT MAX(ID_SISWA)+1 AS 'PRIMARY_id' FROM tb_siswa_lengkap");
		return $query->row_array();
	}

	public function edit_psb($id){
		$this->db->from('v_siswa_lengkap');
		$this->db->where('ID_SISWA',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function edit_siswa_ijazah($where,$data){
		$this->db->where('ID_IJAZAH',$where);
		$this->db->update('ijazah',$data);
    	return $this->db->affected_rows();
	}

	public function edit_siswa($where,$data){
		$this->db->where('ID_SISWA',$where);
		$this->db->update('tb_siswa_lengkap',$data);
    	return $this->db->affected_rows();
	}


}
