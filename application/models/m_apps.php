<?php 
class M_apps extends CI_Model{

	function tampil_data($table){
		 $this->db->from($table);
        $query = $this->db->get();
 
        //cek apakah ada ba
        
        return $query->result(); 
	}
 
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	function delete_data($data,$table){
		$this->db->where($data);
		$this->db->delete($table);
	}
	function edit_data($where,$table){		
		 $this->db->from($table);
        $this->db->where($where);
 
        $query = $this->db->get();
 
        if ($query->num_rows() == 1) {
            return $query->result();
        }
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	

	function check_data($where, $table){
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
        return $query->row();
         
	}

	function check_data_result($where, $table){
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
        return $query->result();
         
	}

	function check_data_num_rows($where, $table){
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
        return $query;
         
	}

}
 ?>