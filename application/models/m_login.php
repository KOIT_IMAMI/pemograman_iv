<?php 

class m_login extends CI_Model{

    public function login($u,$p)
    {
        $p = md5($p);
        $user = $this->db->where('username', $u)
                          ->where('password', $p)
                          ->get('admin');

        if ($user->num_rows()>0) 
        {
            foreach ($user->result() as $row) 
            {
                $sess = array('username' => $row->username, 'nama'=> $row->nama, 'id_admin'=> $row->id_admin);
                $this->session->set_userdata($sess);
            }
            return $user->num_rows();
        }else
        {
            return 0;
        }
    }

    public function logout()
    {
        $data = [
            'username' => null,
            'level'    => null
        ];
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
    }
}