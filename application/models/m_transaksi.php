<?php
class m_transaksi extends CI_Model {
 
    //// ==================================================================================================================================
    // AWAL VARIABEL UNTUK TABEL YANG INGIN DIGUNAKAN
    //variabel m_transaksi
    public $m_transaksi = 'tansaksi';
    public $view_transaksi = 'tansaksi';
 // AWAL FUNCTION PENGAMBILAN DATA transaksi
	function get_alltransaksi() {
        $this->db->from($this->view_transaksi);
        $query = $this->db->get();
 
        //cek apakah ada ba
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

   function getstatustransaksi(){
        return $this->db->query ("SELECT * from tansaksi")->result();
    }
    function getstatustransaksii(){
        return $this->db->query ("SELECT * from tansaksi")->result();
    }
 
    function get_transaksi_byid($id_transaksi) {
        $this->db->from($this->m_transaksi);
        $this->db->where('id', $id_transaksi);
 
        $query = $this->db->get();
 
        if ($query->num_rows() == 1) {
            return $query->result();
        }
    }
 
    function get_insert_transaksi($data){
       $this->db->insert($this->m_transaksi, $data);
       return TRUE;
    }
 
    function get_update_transaksi($id_transaksi,$data) {
 
        $this->db->where('id', $id_transaksi);
        $this->db->update($this->m_transaksi, $data);
 
        return TRUE;
    }
    function del_transaksi($id_transaksi) {
        $this->db->where('id', $id_transaksi);
        $this->db->delete($this->m_transaksi);
        return TRUE;
    }
    function get_all_for_options_transaksi() {
        $result = $this->db->get('m_transaksi');
        $options = array();
        foreach($result->result_array() as $row) {
        $options[$row["id_transaksi"]] = $row["nama_transaksi"];
        }
        return $options;
        }
    // AKHIR FUNCTION DATA transaksi
    
}
?>