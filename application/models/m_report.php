<?php
class m_report extends CI_Model {
 
    //// ==================================================================================================================================
    // AWAL VARIABEL UNTUK TABEL YANG INGIN DIGUNAKAN
    //variabel m_report
   function get_table($tanggal_awal,$tanggal_akhir){
        return $this->db->query ("SELECT tbl.nik, m_karyawan.nama, m_karyawan.tukin_dasar, tbl.potongan,
                                FORMAT(m_karyawan.tukin_dasar * tbl.potongan,2) as total_potongan,
                                tbl.wasete_time_over,
                                m_karyawan.tukin_dasar - (m_karyawan.tukin_dasar * tbl.potongan) as total_tukin_diterima,
                                FORMAT(((tbl.wasete_time_over/TIMESTAMPDIFF(SECOND,  '$tanggal_awal', '$tanggal_akhir' )) * 100),2) as wasete_time_over_pro  -- TANGGAL BETWEEN + 1

                                FROM (SELECT nik, FORMAT(sum(DISTINCT (total_potongan)),5) as potongan , sum(DISTINCT (total_selisih)) as wasete_time_over FROM t_absen  
                                WHERE (tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir')  -- TANGGAL BETWEEN
                                GROUP BY nik) tbl
                                LEFT JOIN m_karyawan on (tbl.nik = m_karyawan.nik)
                                ORDER BY tbl.wasete_time_over")->result();
    }

    function get_grafik($tanggal_awal,$tanggal_akhir){
        $query = "SELECT tbl.nik, m_karyawan.nama, m_karyawan.tukin_dasar, tbl.potongan,
                                FORMAT(m_karyawan.tukin_dasar * tbl.potongan,2) as total_potongan,
                                tbl.wasete_time_over,
                                m_karyawan.tukin_dasar - (m_karyawan.tukin_dasar * tbl.potongan) as total_tukin_diterima,
                                FORMAT(((tbl.wasete_time_over/TIMESTAMPDIFF(SECOND,  '$tanggal_awal', '$tanggal_akhir' )) * 100),2) as wasete_time_over_pro  -- TANGGAL BETWEEN + 1

                                FROM (SELECT nik, FORMAT(sum(DISTINCT (total_potongan)),5) as potongan , sum(DISTINCT (total_selisih)) as wasete_time_over FROM t_absen  
                                WHERE (tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir')  -- TANGGAL BETWEEN
                                GROUP BY nik) tbl
                                LEFT JOIN m_karyawan on (tbl.nik = m_karyawan.nik)
                                ORDER BY tbl.wasete_time_over";
        $data = $this->db->query($query)->result();

        $a = array();
        $b = array();
        if ($data != false){
           
         foreach ($data as $d) { {       
                $b["category"] = $d->nik.'<br>'.$d->nama;
                $b["column-1"] = $d->nik.'-'.$d->nama;
                $b["column-2"] = $d->wasete_time_over_pro;
                array_push($a,$b);
            }
         }

         return $a;
    }

    }
}
?>