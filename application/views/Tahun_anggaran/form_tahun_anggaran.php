 <!-- Main content -->
  <section class="content-header">
          <h1 style="color:#000;"><i class="fa fa-list"></i>
            Data Tahun Anggaran
          </h1>
          <ol class="breadcrumb">

           <p>
           	
           </p>
          </ol>
        </section>
       
        	<section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
            Form Data Tahun Anggaran</h3>
            
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <!-- form start -->
                <form action="#" class="form-horizontal" id="submit_form" method="POST">
                  <input type="hidden" name="id_tahun" value="<?php echo $id_tahun ?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Tahun <span class="required">
                  * </span></label>
                      <div class="col-sm-2">
                        <input type="text" value="<?php echo $tahun ?>" name="tahun" class="form-control" id="inputEmail3" placeholder="Tahun">

                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Keterangan</label>
                      <div class="col-sm-4">
                        <input type="text" value="<?php echo $keterangan ?>" name="keterangan" class="form-control" id="inputPassword3" placeholder="Keterangan">
                      </div>
                    </div>
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <span class="required">
                  * Diisi Sesuai dengan Format Tanggal Exp : 2017</span>
                    <button type="button" id="btn_batal" class="btn btn-danger pull-right"><i class="fa fa-remove"></i> Batal</button>
                    <?php if ($mode=='tambah'): ?>
                      <button type="button" id="btn_simpan" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Simpan Data</button>
                    <?php endif ?>

                    <?php if ($mode=='edit'): ?>
                      <button type="button" id="btn_update" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update Data</button>
                    <?php endif ?>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
             
            </div><!--/.col (right) -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

<script type="text/javascript">
  $(document).ready(function(){
      $("#btn_batal").click(function(){
        event.preventDefault();
          $('#load_content').empty();
          $('#myModal').modal('show');
          setTimeout(function(){
            $('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/data');
            $('#myModal').modal('hide');
          }, 1500);
      });

      $("#btn_simpan").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>tahun_anggaran/simpan',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(){
            swal("SUKSES", "Data Berhasil di Simpan", "success");
            setTimeout(function(){
              $('#load_content').empty();
              $('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/data/ada');
            }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });

      $("#btn_update").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>tahun_anggaran/update_data',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(){
            swal("UPDATE", "Data Berhasil di Simpan", "success");
            setTimeout(function(){
              $('#load_content').empty();
              $('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/data/ada');
            }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });
  });
</script>