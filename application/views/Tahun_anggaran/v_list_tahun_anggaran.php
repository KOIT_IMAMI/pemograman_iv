
        <div id="load_content" >
        	 <!-- Main content -->
      <section class="content-header">

          <h1 style="color:#000;"><i class="fa fa-list"></i>
            Data Tahun Anggaran

          </h1>
          <ol class="breadcrumb">

           <p>
           	
           </p>
          </ol>
        </section>
        <?php if (isset($simpan)): ?>
          <div class="col-md-12">
          <div id="alert-success" class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
          </div>
        </div>
          
        <?php endif ?>
        
        	<section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
            Data Tahun Anggaran</h3>
            <div class="pull-right">
            	<button id="btn_tambah" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</button>
            </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tahun</th>
                        <th>Keterangan</th>
                        <th>Aktif</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; foreach ($data as $row): ?>
                        <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $row->tahun ?></td>
                        <td><?php echo $row->keterangan ?></td>
                        <td>
                          <?php if ($row->aktif=='Y'): ?>
                            <button id="btn_aktif<?php echo $row->id_tahun ?>" class="btn btn-success" onclick="status('<?php echo $row->aktif ?>','<?php echo $row->id_tahun ?>')"><i class="glyphicon glyphicon-ok" ></i></button>
                          <?php endif ?>
                          <?php if ($row->aktif=='N'): ?>
                            <button id="btn_aktif<?php echo $row->id_tahun ?>" class="btn btn-danger" onclick="status('<?php echo $row->aktif ?>','<?php echo $row->id_tahun ?>')"><i class="glyphicon glyphicon-remove" ></i></button>
                          <?php endif ?>
                        </td>
                        <td><button class="btn btn-primary btn-small" onclick="btn_edit('<?php echo $row->id_tahun ?>')"><i class="fa fa-pencil"></i> Edit</button></td>
                      </tr>
                        
                      <?php endforeach ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tahun</th>
                        <th>Keterangan</th>
                        <th>Aktif</th>
                        <th>
                          Action
                        </th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        </div>

  

<script type="text/javascript">
	$(document).ready(function(){
		$("#example1").dataTable();
    $("#alert-success").fadeToggle(3000);
	    $("#btn_tambah").click(function(){
	    	event.preventDefault();
	        $('#load_content').empty();
	        $('#myModal').modal('show');
	        setTimeout(function(){
	        	$('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/form_tambah');
	        	$('#myModal').modal('hide');
	        }, 1500);
	    });

    $("#btn_batal").click(function(){
	    	event.preventDefault();
	        $('#load_content').empty();
	        $('#myModal').modal('show');
	        setTimeout(function(){
	        	$('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/data');
	        	$('#myModal').modal('hide');
	        }, 1500);
	    });
	});

  function btn_edit(id){
    event.preventDefault();
          $('#load_content').empty();
          $('#myModal').modal('show');
          setTimeout(function(){
            $('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/form_edit/'+id);
            $('#myModal').modal('hide');
          }, 1500);
  }

  function status(status,id){
    
    event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>Tahun_anggaran/status_edit/'+id+"/"+status,
          type : "POST",
          data : '',
          dataType : 'json',
          success: function(data){
            if (data.pesan=="simpan") {
              $('#btn_aktif'+id).html('<img width="30" src="<?php echo base_url() ?>assets/preload/loadaer.gif">');
              setTimeout(function(){
                $('#load_content').empty();
                $('#load_content').load('<?php echo base_url() ?>Tahun_anggaran/data');
              }, 1500);
            }else{
              swal("Warning", "Tahun Minimal Harus 1 yang Aktif!!", "warning");
            }
            
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
    
  }
</script>
        