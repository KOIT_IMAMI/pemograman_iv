<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Kelompok Rekening</label>
  <div class="col-sm-3">
    <select name="id_jenis_rekening" class="form-control">
      <option>Pilih Jenis Rekening</option>
      <?php foreach ($db_jenis_rekening as $row): ?>
        <?php
        if ($id_jenis_rekening==$row->id_jenis_rekening) {
            $select = 'selected';
        }else{
          $select = '';
        }  
        ?>
        <option <?php echo $select ?> value="<?php echo $row->id_jenis_rekening ?>"><?php echo $row->jenis_rekening ?></option>
      <?php endforeach ?>
    </select>
  </div>
</div>
