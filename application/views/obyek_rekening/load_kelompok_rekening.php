<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Kelompok Rekening</label>
  <div class="col-sm-3">
    <select name="id_kelompok_rekening" class="form-control" onchange="load_jenis_rekening(this.value)">
      <option>Pilih Kelompok Rekening</option>
      <?php foreach ($db_kelompok_rekening as $row): ?>
        <?php
        if ($id_kelompok_rekening==$row->id_kelompok_rekening) {
            $select = 'selected';
        }else{
          $select = '';
        }  
        ?>
        <option <?php echo $select ?> value="<?php echo $row->id_kelompok_rekening ?>"><?php echo $row->kelompok_rekening ?></option>
      <?php endforeach ?>
    </select>
  </div>
</div>

<script type="text/javascript">
  function load_jenis_rekening(id){
    $('#load_jenis_rekening').load('<?php echo base_url() ?>obyek_rekening/load_jenis_rekening/'+id);
  }
</script>