 <!-- Main content -->
  <section class="content-header">
          <h1 style="color:#000;"><i class="fa fa-list"></i>
            Data Penanggung Jawab
          </h1>
          <ol class="breadcrumb">

           <p>
           	
           </p>
          </ol>
        </section>
       
        	<section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
            Data Penanggung Jawab</h3>
            
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <!-- form start -->
                <form action="#" class="form-horizontal" id="submit_form" method="POST">
                  <input type="hidden" name="id_struk_rekening" value="<?php echo $id_struk_rekening ?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Struk Rekening</label>
                      <div class="col-sm-3">
                        <input type="text" value="<?php echo $struk_rekening ?>" name="struk_rekening" class="form-control" id="inputEmail3" placeholder="Tahun">
                      </div>
                    </div>
                    
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" id="btn_batal" class="btn btn-danger pull-right"><i class="fa fa-remove"></i> Batal</button>
                    <?php if ($mode=='tambah'): ?>
                      <button type="button" id="btn_simpan" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Simpan Data</button>
                    <?php endif ?>

                    <?php if ($mode=='edit'): ?>
                      <button type="button" id="btn_update" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update Data</button>
                    <?php endif ?>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
             
            </div><!--/.col (right) -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

<script type="text/javascript">
  $(document).ready(function(){
      $("#btn_batal").click(function(){
        event.preventDefault();
          $('#load_content').empty();
          $('#myModal').modal('show');
          setTimeout(function(){
            $('#load_content').load('<?php echo base_url() ?>struk_rekening/data');
            $('#myModal').modal('hide');
          }, 1500);
      });

      $("#btn_simpan").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>struk_rekening/simpan',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(){
            swal("SUKSES", "Data Berhasil di Simpan", "success");
            setTimeout(function(){
              $('#load_content').empty();
              $('#load_content').load('<?php echo base_url() ?>struk_rekening/data');
            }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });

      $("#btn_update").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>struk_rekening/update_data',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(){
            swal("UPDATE", "Data Berhasil di Simpan", "success");
            setTimeout(function(){
              $('#load_content').empty();
              $('#load_content').load('<?php echo base_url() ?>struk_rekening/data/ada');
            }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });
  });
</script>