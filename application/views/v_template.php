<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>UNUJA 2018</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url() ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
    <script src="<?php echo base_url(); ?>assets/custom_toast.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
    <script src="<?php echo base_url(); ?>assets/code/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/data.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/drilldown.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    
    <!-- DATA TABES SCRIPT -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/preload/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/preload/css/main.css">

    <?php if (empty($this->session->userdata('id_admin'))): ?>
      <?php redirect('login','refresh') ?>
    <?php endif ?>
    <style type="text/css">
      .modal-dialog {
      width: 98%;
      height: 100%;
    }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
     <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
      </div>
    <div class="wrapper">
      
     <?php $this->load->view('hmf/header'); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
       <?php $this->load->view('hmf/sidebar'); ?>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <center><h4 class="modal-title">Tunggu Sebentar . . .</h4></center>
        </div>
        <div class="modal-body">
          <center>
            <img width="200" src="<?php echo base_url() ?>assets/preload/giphy.gif">
            <p>Sedang Mengambil Data Dari Server . . . . . . . .</p>
          </center>
          
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
      <?php $string = (string)$konten; $this->load->view($string); ?>
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2018 <a href="https://facebook.com/koit.imami">UNUJA</a>.</strong> All rights reserved.
      </footer>
      

      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

     <?php $this->load->view('hmf/footer'); ?>
  </body>
</html>
