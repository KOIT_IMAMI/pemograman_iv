<?php $bulan = array (1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      ); ?>
<section class="content">
    <div class="row">
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h4 style="font-size: 20px;"><?php echo "Rp. ".number_format($jumlah_penggunaan_aktif->jumlah_penggunaan) ?></h4>
                  <span style="color: #fff">Penggunaan</span>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="javascript:;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h4 style="font-size: 20px;"><?php echo "Rp. ".number_format($jumlah_pak_aktif->jumlah_pak) ?></h4>
                  <span>Perubahan Anggaran Keuangan</span>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="javascript:;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h4 style="font-size: 20px;"><?php echo "Rp. ".number_format($jumlah_pendapatan_aktif->jumlah_debit) ?></h4>
                  <span>Pendapatan</span>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="javascript:;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h4 style="font-size: 20px;"><?php echo "Rp. ".number_format($jumlah_pengeluaran_aktif->jumlah_kredit) ?></h4>
                  <span>Pengeluaran</span>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="javascript:;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
          </div><!-- /.row -->
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-6">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              </div><!-- /.box -->

              
            </div><!--/.col (right) -->
            <!-- right column -->
            <div class="col-md-6">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div id="pendapatan_pengeluaran" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              </div><!-- /.box -->

              
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-6">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div id="pengeluaran_pak_bagian" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              </div><!-- /.box -->

              
            </div><!--/.col (right) -->
            <!-- right column -->
            <div class="col-md-6">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div id="anggaran_bagian" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
              </div><!-- /.box -->

              
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

    <script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK ANGGARAN PERTAHUN'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total ANGGARAN'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        <?php foreach ($anggaran_pertahun->result() as $row): ?>
          {
            name: '<?php echo $row->tahun ?>',
            y: <?php echo $row->jml ?>,
            drilldown: '<?php echo $row->tahun ?>'
        },
        <?php endforeach ?>
        ]
    }],
    drilldown: {
        series: [
        <?php foreach ($tahun->result() as $r): ?>
          {
            name: '<?php echo $r->tahun ?>',
            id: '<?php echo $r->tahun ?>',
            data: [
            <?php $obyek_rekening = $this->db->query("SELECT * from v_detail_enggaran_pertahun WHERE id_tahun = '$r->id_tahun'"); ?>
              <?php foreach ($obyek_rekening->result() as $rk): ?>
                [
                    '<?php echo $rk->obyek_rekening ?>',
                    <?php echo $rk->jml ?>
                ],
              <?php endforeach ?>
            ]
        },
        <?php endforeach ?>
         ]
    }
});
    </script>

    <script type="text/javascript">

Highcharts.chart('pendapatan_pengeluaran', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK PENDAPATAN DAN PENGELUARAN'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        <?php foreach ($tahun->result() as $r_tahun): ?>
          '<?php echo $r_tahun->tahun ?>',
        <?php endforeach ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (Rupiah)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} Rupiah</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Pendapatan',
        data: [
        <?php foreach ($pendapatan->result() as $r_pendapatan): ?>
          <?php echo $r_pendapatan->jml."," ?>
        <?php endforeach ?>]

    }, {
        name: 'Pengeluaran',
        data: [<?php foreach ($pengeluaran->result() as $r_pengeluaran): ?>
          <?php echo $r_pengeluaran->jml."," ?>
        <?php endforeach ?>]

    }]
});
    </script>

    <script type="text/javascript">

Highcharts.chart('pengeluaran_pak_bagian', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK PENGELUARAN PAK'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        <?php foreach ($tahun->result() as $r_tahun): ?>
          '<?php echo $r_tahun->tahun ?>',
        <?php endforeach ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (Rupiah)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} Rupiah</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    <?php foreach ($jenis_rekening->result() as $rw): ?>
    <?php $detail_jenis = $this->db->query("SELECT SUM(jumlah_pak) as jumlah, id_tahun, tahun, jenis_rekening FROM v_penyusunan_anggaran WHERE id_jenis_rekening='$rw->id_jenis_rekening' GROUP BY id_tahun"); ?>
      {
        name: '<?php echo $rw->jenis_rekening ?>',
        data: [
        <?php foreach ($tahun->result() as $rw_k): ?>
            <?php $detail_jenis_nominal = $this->db->query("SELECT SUM(jumlah_pak) as jumlah, id_tahun, tahun, jenis_rekening FROM v_penyusunan_anggaran WHERE id_jenis_rekening='$rw->id_jenis_rekening' AND id_tahun = '$rw_k->id_tahun'")->row(); ?>
            <?php if ($detail_jenis_nominal->jumlah=="" or $detail_jenis_nominal->jumlah==null) {
                echo "0,";
            }else{
                echo $detail_jenis_nominal->jumlah.",";
            } ?>
        <?php endforeach ?>
        ]

    },
    <?php endforeach ?>]
});
    </script>


<script type="text/javascript">

Highcharts.chart('anggaran_bagian', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK NOMINAL PENGGUNAAN'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        <?php foreach ($tahun->result() as $r_tahun): ?>
          '<?php echo $r_tahun->tahun ?>',
        <?php endforeach ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (Rupiah)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} Rupiah</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    <?php foreach ($jenis_rekening->result() as $rw): ?>
    <?php $detail_jenis = $this->db->query("SELECT SUM(jumlah_penggunaan) as jumlah, id_tahun, tahun, jenis_rekening FROM v_penyusunan_anggaran WHERE id_jenis_rekening='$rw->id_jenis_rekening' GROUP BY id_tahun"); ?>
      {
        name: '<?php echo $rw->jenis_rekening ?>',
        data: [
        <?php foreach ($tahun->result() as $rw_k): ?>
            <?php $detail_jenis_nominal = $this->db->query("SELECT SUM(jumlah_penggunaan) as jumlah, id_tahun, tahun, jenis_rekening FROM v_penyusunan_anggaran WHERE id_jenis_rekening='$rw->id_jenis_rekening' AND id_tahun = '$rw_k->id_tahun'")->row(); ?>
            <?php if ($detail_jenis_nominal->jumlah=="" or $detail_jenis_nominal->jumlah==null) {
                echo "0,";
            }else{
                echo $detail_jenis_nominal->jumlah.",";
            } ?>
        <?php endforeach ?>
        ]

    },
    <?php endforeach ?>]
});
    </script>

