
        <div id="load_content" >
        	 <!-- Main content -->
      <section class="content-header">

          <h1 style="color:#000;"><i class="fa fa-list"></i>
            Data Kelompok Rekening

          </h1>
          <ol class="breadcrumb">

           <p>
           	
           </p>
          </ol>
        </section>
        
        
        	<section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
           Data Kelompok Rekening</h3>
            <div class="pull-right">
            	<button id="btn_tambah" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</button>
            </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Penanggung Jawab</th>
                        <th>Aktif</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; foreach ($data as $row): ?>
                        <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $row->id_kelompok_rekening ?></td>
                        <td><?php echo $row->kelompok_rekening ?></td>
                        <td>
                          <?php if ($row->status_kelompok_rekening=='Y'): ?>
                            <button id="btn_aktif<?php echo $row->id_kelompok_rekening ?>" class="btn btn-success" onclick="status('<?php echo $row->status_kelompok_rekening ?>','<?php echo $row->id_kelompok_rekening ?>')"><i class="glyphicon glyphicon-ok" ></i></button>
                          <?php endif ?>
                          <?php if ($row->status_kelompok_rekening=='N'): ?>
                            <button id="btn_aktif<?php echo $row->id_kelompok_rekening ?>" class="btn btn-danger" onclick="status('<?php echo $row->status_kelompok_rekening ?>','<?php echo $row->id_kelompok_rekening ?>')"><i class="glyphicon glyphicon-remove" ></i></button>
                          <?php endif ?>
                                         
                        </td>
                        <td><button class="btn btn-primary btn-small" onclick="btn_edit('<?php echo $row->id_kelompok_rekening ?>')"><i class="fa fa-pencil"></i> Edit</button></td>
                      </tr>
                        
                      <?php endforeach ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Penanggung Jawab</th>
                        <th>Aktif</th>
                        <th>
                          Action
                        </th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        </div>

  

<script type="text/javascript">
	$(document).ready(function(){
		$("#example1").dataTable();
    $("#alert-success").fadeToggle(3000);
	    $("#btn_tambah").click(function(){
	    	event.preventDefault();
	        $('#load_content').empty();
	        $('#myModal').modal('show');
	        setTimeout(function(){
	        	$('#load_content').load('<?php echo base_url() ?>kelompok_rekening/form_tambah');
	        	$('#myModal').modal('hide');
	        }, 1500);
	    });

    $("#btn_batal").click(function(){
	    	event.preventDefault();
	        $('#load_content').empty();
	        $('#myModal').modal('show');
	        setTimeout(function(){
	        	$('#load_content').load('<?php echo base_url() ?>kelompok_rekening/data');
	        	$('#myModal').modal('hide');
	        }, 1500);
	    });
	});

  function btn_edit(id){
    event.preventDefault();
          $('#load_content').empty();
          $('#myModal').modal('show');
          setTimeout(function(){
            $('#load_content').load('<?php echo base_url() ?>kelompok_rekening/form_edit/'+id);
            $('#myModal').modal('hide');
          }, 1500);
  }

  function status(status,id){
    $('#btn_aktif'+id).html('<img width="30" src="<?php echo base_url() ?>assets/preload/loadaer.gif">');
    event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>kelompok_rekening/status_edit/'+id+"/"+status,
          type : "POST",
          data : '',
          dataType : 'json',
          success: function(){
            setTimeout(function(){
              $('#load_content').empty();
              $('#load_content').load('<?php echo base_url() ?>kelompok_rekening/data');
            }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
    
  }
</script>
        