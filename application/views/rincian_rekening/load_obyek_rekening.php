<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Obyek Rekening</label>
  <div class="col-sm-3">
    <select name="id_obyek_rekening" class="form-control">
      <option>Pilih Obyek Rekening</option>
      <?php foreach ($db_obyek_rekening as $row): ?>
        <option value="<?php echo $row->id_obyek_rekening ?>"><?php echo $row->obyek_rekening ?></option>
      <?php endforeach ?>
    </select>
  </div>
</div>