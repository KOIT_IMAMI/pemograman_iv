<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Kelompok Rekening</label>
  <div class="col-sm-3">
    <select name="id_obyek_rekening" class="form-control" onchange="load_obyek_rekening(this.value)">
      <option>Pilih Obyek Rekening</option>
      <?php foreach ($db_obyek_rekening as $row): ?>
        <option value="<?php echo $row->id_obyek_rekening ?>"><?php echo $row->obyek_rekening ?></option>
      <?php endforeach ?>
    </select>
  </div>
</div>

<script type="text/javascript">
  function load_obyek_rekening(id){
    $('#load_obyek_rekening').load('<?php echo base_url() ?>obyek_rekening/load_obyek_rekening/'+id);
  }
</script>