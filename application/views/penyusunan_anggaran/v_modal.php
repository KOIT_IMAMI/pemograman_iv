<?php 
	if (isset($table)) {
		if ($table->num_rows() > 0 ) {
			$r_data = $table->row();
		}
	}
	
 ?>

<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="peralatan_modal" tabindex="-1" data-backdrop="static" data-keyboard="true"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 720px">
		<div class="modal-content">
			<form role="form" id="submit_form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">PENYUSUNAN ANGGARAN </h4>
			</div>
			<div class="modal-body">
				
					<div class="form-body">
						<p>
							<div class="row">
								<input type="hidden" name="id_pend_peng" value="<?php echo $id_pend_peng ?>">
								<div class="form-group">
									<label class="control-label col-md-3">Tahun Anggaran <span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<select name="id_tahun" class="form-control">
											<option>Pilih Tahun Anggaran</option>
											<?php foreach ($tahun as $row): ?>
												<?php
												if ($id_tahun==$row->id_tahun) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
												<option <?php echo $select ?> value="<?php echo $row->id_tahun ?>"><?php echo $row->tahun ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</p>
						<p>
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-3">Penanggung Jawab <span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<select name="id_penjab" class="form-control">
										<option>Pilih Penanggung Jawab</option>\
										<?php foreach ($penjab as $row): ?>
											<?php
												if ($id_penjab==$row->id_penjab) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
											<option <?php echo $select ?> value="<?php echo $row->id_penjab ?>"><?php echo $row->penjab ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
						</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Struk Rekening <span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<select name="id_struk_rekening" class="form-control" onchange="load_kelompok_rekening(this.value)">
											<option>Pilih Struk Rekening</option>
											
											<?php foreach ($struk_rekening as $row): ?>
												<?php
												if ($id_struk_rekening==$row->id_struk_rekening) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
												<option <?php echo $select ?> value="<?php echo $row->id_struk_rekening ?>"><?php echo $row->id_struk_rekening."-".$row->struk_rekening ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</p>
						 <div id="load_kelompok_rekening">
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Kelompok Rekening <span class="required">
									* </span>
									</label>
									<div class="col-md-5">
										<select name="id_kelompok_rekening" class="form-control" onchange="load_jenis_rekening(this.value)">
											<option>Pilih Kelompok Rekening</option>
											<?php foreach ($kelompok_rekening as $row): ?>
												<?php
												if ($id_kelompok_rekening==$row->id_kelompok_rekening) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
												<option value="<?php echo $row->id_kelompok_rekening ?>" <?php echo $select ?>><?php echo $row->id_kelompok_rekening."-".$row->kelompok_rekening ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</p>
						</div>

						 <div id="load_jenis_rekening">
						<p>
						<div class="row">
							<div class="form-group">
								<label class="control-label col-md-3">jenis Rekening <span class="required">
								* </span>
								</label>
								<div class="col-md-6">
									<select name="id_jenis_rekening" class="form-control"  onchange="load_obyek_rekening(this.value)">
										<option>Pilih jenis Rekening</option>
										<?php foreach ($jenis_rekening as $row): ?>
											<?php
												if ($id_jenis_rekening==$row->id_jenis_rekening) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
											<option value="<?php echo $row->id_jenis_rekening ?>" <?php echo $select ?>><?php echo $row->id_jenis_rekening."-".$row->jenis_rekening ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
						</div>
						</p>
						</div>

						 <div id="load_obyek_rekening">
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">obyek Rekening <span class="required">
									* </span>
									</label>
									<div class="col-md-7">
										<select name="id_obyek_rekening" class="form-control"  onchange="load_rincian_rekening(this.value)">
											<option>Pilih obyek Rekening</option>
											<?php foreach ($obyek_rekening as $row): ?>
												<?php
												if ($id_obyek_rekening==$row->id_obyek_rekening) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
												<option value="<?php echo $row->id_obyek_rekening ?>" <?php echo $select ?>><?php echo $row->id_obyek_rekening."-".$row->obyek_rekening ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</p>
						</div>

						 <div id="load_rincian_rekening">
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">rincian Rekening <span class="required">
									* </span>
									</label>
									<div class="col-md-8">
										<select name="id_rincian_rekening" class="form-control">
											<option>Pilih rincian Rekening</option>
											<?php foreach ($rincian_rekening as $row): ?>
												<?php
												if ($id_rincian_rekening==$row->id_rincian_rekening) {
												  	$select = 'selected';
												}else{
													$select = '';
												}  
												?>
												<option value="<?php echo $row->id_rincian_rekening ?>" <?php echo $select ?>><?php echo $row->id_rincian_rekening."-".$row->rincian_rekening ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</p>
						</div>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Uraian <span class="required">
									* </span>
									</label>
									<div class="col-md-8">
										<input value="<?php echo $uraian ?>" type="text" class="form-control" name="uraian" id="uraian" placeholder="Uraian">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Vol Penggunaan<span class="required">
									* </span>
									</label>
									<div class="col-md-2">
										<input value="<?php echo $vol_penggunaan ?>" id="vol_penggunaan" type="number" class="form-control" name="vol_penggunaan" placeholder="Jenis Alat" onkeyup="jumlah_penggunaan()">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Satuan Penggunaan<span class="required">
									* </span>
									</label>
									<div class="col-md-3">
										<input value="<?php echo $satuan_penggunaan ?>" type="text" class="form-control" name="satuan_penggunaan" placeholder="Satuan Penggunaan">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Harga Penggunaan<span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<input value="<?php echo $harga_penggunaan ?>" id="harga_penggunaan" type="number" class="form-control" name="harga_penggunaan" id="alat_jenis" placeholder="Harga Penggunaan" onkeyup="jumlah_penggunaan()">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Jumlah Penggunaan<span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<input type="number" value="<?php echo $jumlah_penggunaan ?>" class="form-control" name="alat_jenis" id="jumlah_penggunaan" placeholder="Jumlah Penggunaan" readonly>
									</div>
								</div>
							</div>
						</p>

						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Vol PAK<span class="required">
									* </span>
									</label>
									<div class="col-md-2">
										<input type="number" value="<?php echo $vol_pak ?>" class="form-control" name="vol_pak" id="vol_pak" placeholder="Vol PAK" onkeyup="jumlah_pak()">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Satuan PAK<span class="required">
									* </span>
									</label>
									<div class="col-md-3">
										<input value="<?php echo $satuan_pak ?>" type="text" class="form-control" name="satuan_pak" id="satuan_pak" placeholder="Satuan PAK">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Harga PAK<span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<input value="<?php echo $harga_pak ?>" type="number" class="form-control" name="harga_pak" id="harga_pak" placeholder="Harga PAK" onkeyup="jumlah_pak()">
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Jumlah PAK<span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<input value="<?php echo $jumlah_pak ?>" type="number" id="jumlah_pak" class="form-control" name="alat_jenis" id="alat_jenis" placeholder="Jumlah Penggunaan" readonly>
									</div>
								</div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
								    <label class="control-label col-md-3">Tanggal Realisasi <span class="required">
								    * </span>
								    </label>
								    <div class="col-md-8">
								      <div class="input-icon">
								        <input type="date" value="<?php echo $tgl_realiasi ?>" class="person_bird_day form-control required" value="" name="tgl_realiasi" placeholder="Tanggal Mutasi Siswa">
								        <span class="help-block">Format Tanggal: dd/mm/yyyy - contoh: 30/12/1990</span>
								      </div>
								    </div>
								  </div>
							</div>
						</p>
						<p>
							<div class="row">
								<div class="form-group">
									<label class="control-label col-md-3">Debit/Kredit <span class="required">
									* </span>
									</label>
									<div class="col-md-4">
										<select name="jenis" class="form-control">
											<option>Pilih rincian Rekening</option>
											<option <?php echo $jenis == 'D' ?  'selected' : ""; ?> value="D">Debit</option>
											<option <?php echo $jenis == 'K' ?  'selected' : ""; ?> value="K">Kredit</option>
										</select>
									</div>
								</div>
							</div>
						</p>
						
					</div>
			</div>

			<div class="modal-footer">
				<?php if ($mode=='tambah'): ?>
					<button type="button" class="btn btn-success" onclick="simpan()" ><i class="fa fa-save"></i> Simpan</button>
				<?php endif ?>
				<?php if ($mode=='update'): ?>
					<button type="button" class="btn btn-success" onclick="update_data()" ><i class="fa fa-save"></i> Update</button>
				<?php endif ?>
				
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
	$("#peralatan_modal").modal('show');

	function load_kelompok_rekening(id){
    $('#load_kelompok_rekening').load('<?php echo base_url() ?>penyusunan_anggaran/load_kelompok_rekening/'+id);
  }

  function load_jenis_rekening(id){
    $('#load_jenis_rekening').load('<?php echo base_url() ?>penyusunan_anggaran/load_jenis_rekening/'+id);
  }

  function load_obyek_rekening(id){
    $('#load_obyek_rekening').load('<?php echo base_url() ?>penyusunan_anggaran/load_obyek_rekening/'+id);
  }
  function load_rincian_rekening(id){
    $('#load_rincian_rekening').load('<?php echo base_url() ?>penyusunan_anggaran/load_rincian_rekening/'+id);
  }

  function jumlah_penggunaan(){
  	var vol_penggunaan = $('#vol_penggunaan').val();
  	var harga_penggunaan = $('#harga_penggunaan').val();
  	var jumlah_penggunaan = vol_penggunaan*harga_penggunaan;
  	if (harga_penggunaan== "") {
  		$('#jumlah_penggunaan').val(vol_penggunaan);
  	}else{
  		$('#jumlah_penggunaan').val(jumlah_penggunaan);
  	}
  	
  }

   function jumlah_pak(){
  	var vol_pak = $('#vol_pak').val();
  	var harga_pak = $('#harga_pak').val();
  	var jumlah_pak = vol_pak*harga_pak;
  	if (harga_pak== "") {
  		$('#jumlah_pak').val(vol_pak);
  	}else{
  		$('#jumlah_pak').val(jumlah_pak);
  	}
  	
  }

  function simpan(){
  	event.preventDefault();
    $.ajax({
      url : '<?php echo base_url() ?>penyusunan_anggaran/simpan',
      type : "POST",
      data : $('#submit_form').serialize(),
      dataType : 'json',
      success: function(){
      	setTimeout(function() {
         swal({
          title: "SIMPAN DATA!!",
          text: "DATA BERHASIL DI SIMPAN!!!",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
        },
        function(){
          setTimeout(function(){
          	$("#peralatan_modal").modal('hide');
            location.reload();
          }, 500);
        });
      },2000);
      },
      error: function(jqXHR, textStatus, errorThrown){
        swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

      }
    });
  }

  function update_data(){
  	event.preventDefault();
    $.ajax({
      url : '<?php echo base_url() ?>penyusunan_anggaran/update_data',
      type : "POST",
      data : $('#submit_form').serialize(),
      dataType : 'json',
      success: function(data){
      	setTimeout(function() {
         swal({
          title: "UPDATE DATA!!",
          text: "DATA BERHASIL DI UPDATE!!!",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
        },
        function(){
          setTimeout(function(){
          	$("#peralatan_modal").modal('hide');
            location.reload();
          }, 500);
        });
      },2000);
      },
      error: function(jqXHR, textStatus, errorThrown){
        swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

      }
    });
  }
</script>

