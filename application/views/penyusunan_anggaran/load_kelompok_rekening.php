<p>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-md-3">Kelompok Rekening <span class="required">
			* </span>
			</label>
			<div class="col-md-5">
				<select name="id_kelompok_rekening" class="form-control" onchange="load_jenis_rekening(this.value)">
					<option>Pilih Kelompok Rekening</option>
					<?php foreach ($kelompok_rekening as $row): ?>
						<option value="<?php echo $row->id_kelompok_rekening ?>"><?php echo $row->id_kelompok_rekening."-".$row->kelompok_rekening ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</p>