<?php
function tanggal_indo($tanggal)
{
  $bulan = array (1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      );
  $split = explode('-', $tanggal);
  if (count($split)<3) {
    return "00-00-0000";
  }else{
    return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
  }
  
}
?>

<div id="v_modal_id">
</div>
            <div class="col-md-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
           Data Anggaran Tahun</h3>
            <div class="pull-right">
              <button id="btn_tambah" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</button>
            </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Rekening</th>
                        <th>Uraian</th>
                        <th>Vol</th>
                        <th>Satuan</th>
                        <th>Harga Satuan</th>
                        <th>Jumlah</th>
                        <th>Tanggal <br> Realisasi</th>
                        <th>Vol PAK</th>
                        <th>Satuan <br>PAK</th>
                        <th>Harga <br>PAK</th>
                        <th>Jumlah</th>
                        <th>Jenis</th>
                        <th>Tahun</th>
                        <th>Bertambah/<br>Berkurang</th>
                        
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; foreach ($data as $row): ?>
                      <?php if ($row->hasil_akhir<0) {
                        $bg_color = '#e50016';
                        $info = '<i class="fa fa-minus-square"></i>';
                      }else if ($row->hasil_akhir==0) {
                        $bg_color = '';
                        $info = '';
                      }else if ($row->hasil_akhir>0) {
                        $bg_color = '#0bb403';
                        $info = '<i class="fa fa-plus-square"></i>';
                      } ?>
                        <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $row->id_rincian_rekening ?></td>
                        <td><?php echo $row->uraian ?></td>
                        <td><?php echo $row->vol_penggunaan ?></td>
                        <td><?php echo $row->satuan_penggunaan ?></td>
                        <td>
                         <?php echo number_format($row->harga_penggunaan) ?>
                        </td>
                        <td>
                         <?php echo number_format($row->jumlah_penggunaan) ?>
                        </td>
                        <td>
                          <?php echo tanggal_indo($row->tgl_realiasi) ?>
                        </td>
                        <td><?php echo $row->vol_pak ?></td>
                        <td><?php echo $row->satuan_pak ?></td>
                        <td><?php echo number_format($row->harga_pak) ?></td>
                        <td><?php echo number_format($row->jumlah_pak) ?></td>
                        <td><?php echo $row->jenis ?></td>
                        <td><?php echo $row->tahun ?></td>
                        <td style="color: <?php echo $bg_color ?>">
                          <?php if ($row->hasil_akhir<0) {
                            echo $info." (".abs($row->hasil_akhir).")";
                          }else{
                             echo $info." ".number_format($row->hasil_akhir);
                          } ?>  
                          </td>
                          
                        <td>
                          <button class="btn btn-primary btn-small" onclick="edit_data('<?php echo $row->id_pend_peng ?>')"><i class="fa fa-pencil"></i></button>
                          <button class="btn btn-danger btn-small" onclick="Hapus('<?php echo $row->id_pend_peng ?>')"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                        
                      <?php endforeach ?>
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#example1').DataTable( {
        "scrollX": true
    });

    $("#btn_tambah").click(function(){
        event.preventDefault();
         $("#v_modal_id").load('<?php echo base_url() ?>Penyusunan_anggaran/modal_add');
      });
  });

  function edit_data(id){
    event.preventDefault();
    $("#v_modal_id").load('<?php echo base_url() ?>Penyusunan_anggaran/modal_edit/'+id);
  }

</script>
<script type="text/javascript">
                 function Hapus(id){
        swal({
        title: "Kamu Yakin?",
        text: "Data Akan Di Hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Hapus!',
        closeOnConfirm: true,
        showLoaderOnConfirm: true
      },function(){
        $.ajax({
            url : '<?php echo base_url() ?>Penyusunan_anggaran/hapus/'+id,
            type : "GET",
            dataType : 'json',
            success: function(data){
               $('#peralatan_modal').modal('hide');
               $('#load_anggaran').load('<?php echo base_url() ?>Penyusunan_anggaran/data/'+data.tahun);   
            },
            error: function(jqXHR, textStatus, errorThrown){
                alert('Error adding / Update data');

            }
        });
      }
      )
    }
            </script>
