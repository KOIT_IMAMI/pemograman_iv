 <!-- Main content -->
  <section class="content-header">
          <h1 style="color:#000;"><i class="fa fa-list"></i>
            Data Penanggung Jawab
          </h1>
          <ol class="breadcrumb">

           <p>
           	
           </p>
          </ol>
        </section>
       
        	<section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-list"></i>
            Data Penanggung Jawab</h3>
            
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <!-- form start -->
                <form action="#" class="form-horizontal" id="submit_form" method="POST">
                  <input type="hidden" name="id_kelompok_rekening" value="<?php echo $id_kelompok_rekening ?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Struk Rekening</label>
                      <div class="col-sm-3">
                        <select name="id_struk_rekening" class="form-control">
                          <option>Pilih Kelompok Rekening</option>
                          <?php foreach ($db_struk as $row): ?>
                            <?php
                            if ($id_struk_rekening==$row->id_struk_rekening) {
                                $select = 'selected';
                            }else{
                              $select = '';
                            }  
                            ?>
                            <option <?php echo $select ?> value="<?php echo $row->id_struk_rekening ?>"><?php echo $row->struk_rekening ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>


                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Kelompok Rekening</label>
                      <div class="col-sm-5">
                        <input type="text" value="<?php echo $kelompok_rekening ?>" name="kelompok_rekening" class="form-control" id="inputEmail3" placeholder="Kelompok Rekening">
                      </div>
                    </div>
                    
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" id="btn_batal" class="btn btn-danger pull-right"><i class="fa fa-remove"></i> Batal</button>
                    <?php if ($mode=='tambah'): ?>
                      <button type="button" id="btn_simpan" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Simpan Data</button>
                    <?php endif ?>

                    <?php if ($mode=='edit'): ?>
                      <button type="button" id="btn_update" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update Data</button>
                    <?php endif ?>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
             
            </div><!--/.col (right) -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

<script type="text/javascript">
  $(document).ready(function(){
      $("#btn_batal").click(function(){
        event.preventDefault();
          $('#load_content').empty();
          $('#myModal').modal('show');
          setTimeout(function(){
            $('#load_content').load('<?php echo base_url() ?>kelompok_rekening/data');
            $('#myModal').modal('hide');
          }, 1500);
      });

      $("#btn_simpan").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>kelompok_rekening/simpan',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(data){
            swal("SUKSES", "Data Berhasil di Simpan", "success");
                setTimeout(function(){
                  $('#load_content').empty();
                  $('#load_content').load('<?php echo base_url() ?>kelompok_rekening/data');
                }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });

      $("#btn_update").click(function(){
        event.preventDefault();
        $.ajax({
          url : '<?php echo base_url() ?>kelompok_rekening/update_data',
          type : "POST",
          data : $('#submit_form').serialize(),
          dataType : 'json',
          success: function(data){
            swal("Update", "Data Berhasil di Simpan", "success");
                setTimeout(function(){
                  $('#load_content').empty();
                  $('#load_content').load('<?php echo base_url() ?>kelompok_rekening/data');
                }, 1500);
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
      });
  });
</script>