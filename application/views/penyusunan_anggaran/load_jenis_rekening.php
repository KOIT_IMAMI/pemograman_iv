<p>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-md-3">jenis Rekening <span class="required">
			* </span>
			</label>
			<div class="col-md-6">
				<select name="id_jenis_rekening" class="form-control" onchange="load_obyek_rekening(this.value)">
					<option>Pilih jenis Rekening</option>
					<?php foreach ($jenis_rekening as $row): ?>
						<option value="<?php echo $row->id_jenis_rekening ?>"><?php echo $row->id_jenis_rekening."-".$row->jenis_rekening ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</p>