<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            PENYUSUNAN ANGGARAN
            <small></small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Filter Data</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-1 control-label"></label>
                      <div class="col-sm-10">
                        Silahkan isi beberapa option atau keseluruhan form untuk menampilkan data secara spesifik.
                      </div>
                    </div>
                    <div class="col-md-4">
                      
                    
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-6 control-label">Tahun Anggaran</label>
                      <div class="col-sm-4">
                        <select class="form-control" name="id_tahun" id="id_tahun">
                          <option value="semua">Semua</option>
                          <?php foreach ($db_tahun as $row): ?>
                            <option value="<?php echo $row->id_tahun ?>"><?php echo $row->tahun ?></option>
                          <?php endforeach ?>
                          
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-6 control-label">Bertambah / Berkurang</label>
                      <div class="col-sm-6">
                        <select class="form-control select2me" name="hasil_akhir" id="hasil_akhir">
                          <option value="">Pilih</option>
                          <option value="semua">Semua</option>
                          <option value="1">Positif</option>
                          <option value="-1">Negatif</option>
                        </select>
                      </div>
                    </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-5 control-label">Struk Rekening</label>
                      <div class="col-sm-7">
                        <select class="form-control" name="id_struk_rekening" id="id_struk_rekening" onchange="load_field_kelompok(this.value)">
                          <option value="">Pilih</option>
                          <?php foreach ($struk_rekening as $row): ?>
                            <option value="<?php echo $row->id_struk_rekening ?>"><?php echo $row->struk_rekening ?></option>
                          <?php endforeach ?>
                          
                        </select>
                      </div>
                    </div>

                    <div id="load_kelompok_rekening">
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-5 control-label">Kelompok Rekening</label>
                        <div class="col-sm-7">
                          <select class="form-control" name="id_kelompok_rekening" id="id_kelompok_rekening" disabled>
                            <option value="">Pilih</option>
                          </select>
                        </div>
                      </div>
                    </div>
                                         
                    </div>
                    <div class="col-md-4">
                      <div id="load_jenis_rekening">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-5 control-label">Jenis Rekening</label>
                            <div class="col-sm-7">
                              <select class="form-control" name="id_jenis_rekening" id="id_jenis_rekening" disabled>
                                <option value="">Pilih</option>
                              </select>
                            </div>
                        </div>
                      </div>

                      <div id="load_obyek_rekening">
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-5 control-label">Obyek Rekening</label>
                          <div class="col-sm-7">
                            <select class="form-control" name="id_obyek_rekening" id="id_obyek_rekening" disabled>
                              <option value="">Pilih</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button onclick="reset_data()" type="button" class="btn btn-danger pull-right"> <i class="fa fa-remove"></i> Reset Data</button>
                    <button type="button" onclick="load_v_anggran()" class="btn btn-info pull-right"> <i class="fa fa-search"></i> Filter Data</button>
                    
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->

              <div id="load_anggaran"></div>
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#load_anggaran').load('<?php echo base_url() ?>Penyusunan_anggaran/data_all');
  });
  function load_v_anggran(){
    event.preventDefault();
    var id_tahun = $('#id_tahun').val();
    var id_struk_rekening = $('#id_struk_rekening').val();
    var id_kelompok_rekening = $('#id_kelompok_rekening').val();
    var id_jenis_rekening = $('#id_jenis_rekening').val();
    var id_obyek_rekening = $('#id_obyek_rekening').val();
    var hasil_akhir = $('#hasil_akhir').val();
    $('#load_anggaran').empty();
    $('#myModal').modal('show');
    $('#load_anggaran').load('<?php echo base_url() ?>Penyusunan_anggaran/data_filter',{'id_tahun':id_tahun,'id_struk_rekening':id_struk_rekening,'id_kelompok_rekening':id_kelompok_rekening,'id_jenis_rekening':id_jenis_rekening,'id_obyek_rekening':id_obyek_rekening,'hasil_akhir':hasil_akhir});
    // if (nilai=='semua') {
      setTimeout(function(){
    //     $('#load_anggaran').load('<?php echo base_url() ?>Penyusunan_anggaran/data_all');
        $('#myModal').modal('hide');
      }, 1500);
    // }else{
    //   setTimeout(function(){
    //     $('#load_anggaran').load('<?php echo base_url() ?>Penyusunan_anggaran/data/'+nilai);
    //     $('#myModal').modal('hide');
    //   }, 1500);
    // }
    
  }

  function reset_data(){
    $('#load_anggaran').empty();
    $('#myModal').modal('show');
    setTimeout(function(){
      $('#myModal').modal('hide');
    }, 1500);
  }
</script>

<script type="text/javascript">
  function load_field_kelompok(id){
    // disable fielad master rekening
    if (id=="") {
    }else{
      $('#load_kelompok_rekening').load('<?php echo base_url() ?>Penyusunan_anggaran/load_field_kelompok/'+id);
      $('#id_jenis_rekening').attr('disabled','disabled');
      $('#id_jenis_rekening').val('');
      $('#id_obyek_rekening').attr('disabled','disabled');
      $('#id_obyek_rekening').val('');
    }
    

  }
</script>