<p>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-md-3">rincian Rekening <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<select name="id_rincian_rekening" class="form-control">
					<option>Pilih rincian Rekening</option>
					<?php foreach ($rincian_rekening as $row): ?>
						<option value="<?php echo $row->id_rincian_rekening ?>"><?php echo $row->id_rincian_rekening."-".$row->rincian_rekening ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</p>