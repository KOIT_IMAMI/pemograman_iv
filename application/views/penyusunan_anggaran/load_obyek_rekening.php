<p>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-md-3">obyek Rekening <span class="required">
			* </span>
			</label>
			<div class="col-md-7">
				<select name="id_obyek_rekening" class="form-control" onchange="load_rincian_rekening(this.value)">
					<option>Pilih obyek Rekening</option>
					<?php foreach ($obyek_rekening as $row): ?>
						<option value="<?php echo $row->id_obyek_rekening ?>"><?php echo $row->id_obyek_rekening."-".$row->obyek_rekening ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</p>