<div class="form-group">
	<label for="inputPassword3" class="col-sm-5 control-label">Obyek Rekening</label>
	<div class="col-sm-7">
	  <select class="form-control" name="id_obyek_rekening" id="id_obyek_rekening">
	    <option value="">Pilih</option>
	    <?php foreach ($obyek_rekening as $row): ?>
	    	<option value="<?php echo $row->id_obyek_rekening ?>"><?php echo $row->obyek_rekening ?></option>
	    <?php endforeach ?>
	  </select>
	</div>
</div>