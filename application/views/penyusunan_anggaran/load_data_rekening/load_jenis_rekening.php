<div class="form-group">
	<label for="inputPassword3" class="col-sm-5 control-label">Jenis Rekening</label>
	<div class="col-sm-7">
	  <select class="form-control" name="id_jenis_rekening" id="id_jenis_rekening" onchange="load_field_obyek(this.value)">
	    <option value="">Pilih</option>
	    <?php foreach ($jenis_rekening as $row): ?>
	    	<option value="<?php echo $row->id_jenis_rekening ?>"><?php echo $row->jenis_rekening ?></option>
	    <?php endforeach ?>
	  </select>
	</div>
</div>

<script type="text/javascript">
  function load_field_obyek(id){
  	if (id!="") {
    $('#load_obyek_rekening').load('<?php echo base_url() ?>Penyusunan_anggaran/load_field_obyek/'+id);
    // disable fielad master rekening
	}else{
		$('#id_obyek_rekening').attr('disabled','disabled');
	    $('#id_obyek_rekening').val('');
	}
}
</script>