<div class="form-group">
	<label for="inputPassword3" class="col-sm-5 control-label">Kelompok Rekening</label>
	<div class="col-sm-7">
	  <select class="form-control" name="id_kelompok_rekening" id="id_kelompok_rekening" onchange="load_field_jenis(this.value)">
	    <option value="">Pilih</option>
	    <?php foreach ($kelompok_rekening as $row): ?>
	    	<option value="<?php echo $row->id_kelompok_rekening ?>"><?php echo $row->kelompok_rekening ?></option>
	    <?php endforeach ?>
	  </select>
	</div>
</div>

<script type="text/javascript">
  function load_field_jenis(id){

  	if (id!="") {
  		$('#load_jenis_rekening').load('<?php echo base_url() ?>Penyusunan_anggaran/load_field_jenis/'+id);
	    // disable fielad master rekening
	    $('#id_obyek_rekening').attr('disabled','disabled');
	    $('#id_obyek_rekening').val('');
  	}else{
  		$('#id_jenis_rekening').attr('disabled','disabled');
	    $('#id_jenis_rekening').val('');
  		$('#id_obyek_rekening').attr('disabled','disabled');
	    $('#id_obyek_rekening').val('');
  	}
  }
</script>