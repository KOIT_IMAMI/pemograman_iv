<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>UNUJA 2018</title>
  <meta name="keywords" content="Bootstrap 3 Admin Dashboard Template Theme" />
  <meta name="description" content="AdminDesigns - Bootstrap 3 Admin Dashboard Theme">
  <meta name="author" content="AdminDesigns">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php if (!empty($this->session->userdata('id_admin'))): ?>
      <?php redirect('dashboard','refresh') ?>
    <?php endif ?>


  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/login/assets/img/favicon.ico">
  <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content">

        <div class="admin-form theme-info" id="login1">

          <div class="row mb15 table-layout">

            <div class="col-xs-6 va-m pln">
              <a href="" title="Return to Dashboard">

                <img src="<?php echo base_url() ?>assets/login/assets/img/logos/logounuja.png" title="AdminDesigns Logo" class="img-responsive w250">
              </a>
            </div>

            <div class="col-xs-6 text-right va-b pr5">
              <div class="login-links">
                
              </div>

            </div>

          </div>

          <div class="panel panel-info mt10 br-n">

            <div class="panel-heading heading-border bg-white">
              <span class="panel-title hidden">
                <i class="fa fa-sign-in"></i>Register</span>
              <div class="section row mn">
                <div class="col-sm-12" align="center">
                  <span style="color: #000;font-weight: bold;text-align: center;font-size: 20px;"> HALAMAN LOGIN</span>
                </div>

              </div>
            </div>

            <!-- end .form-header section -->
            <form method="post" action="/" id="contact">
              <div class="panel-body bg-light p30">
                <div class="row">
                  <div class="col-sm-12 pr30">

                    <div class="section row hidden">
                      <div class="col-md-4">
                        <a href="#" class="button btn-social facebook span-left mr5 btn-block">
                          <span>
                            <i class="fa fa-facebook"></i>
                          </span>Facebook</a>
                      </div>
                      <div class="col-md-4">
                        <a href="#" class="button btn-social twitter span-left mr5 btn-block">
                          <span>
                            <i class="fa fa-twitter"></i>
                          </span>Twitter</a>
                      </div>
                      <div class="col-md-4">
                        <a href="#" class="button btn-social googleplus span-left btn-block">
                          <span>
                            <i class="fa fa-google-plus"></i>
                          </span>Google+</a>
                      </div>
                    </div>

                    <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Username</label>
                      <label for="username" class="field prepend-icon">
                        <input type="text" name="username" id="username" class="gui-input" placeholder="Enter username" autocomplete="none">
                        <label for="username" class="field-icon">
                          <i class="fa fa-user"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->

                    <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Password</label>
                      <label for="password" class="field prepend-icon">
                        <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
                        <label for="password" class="field-icon">
                          <i class="fa fa-lock"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->

                  </div>
                  
                </div>
              </div>
              <!-- end .form-body section -->
              <div class="panel-footer clearfix p10 ph15">
                <button type="button" id="btn_login" class="button btn-primary mr10 pull-right">Sign In</button>
                <label class="switch ib switch-primary pull-left input-align mt10">
                  <input type="checkbox" name="remember" id="remember" checked>
                  <label for="remember" data-on="YES" data-off="NO"></label>
                  <span>Remember me</span>
                </label>
              </div>
              <!-- end .form-footer section -->
            </form>
          </div>
        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="<?php echo base_url() ?>assets/login/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="<?php echo base_url() ?>assets/login/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- CanvasBG Plugin(creates mousehover effect) -->
  <script src="<?php echo base_url() ?>assets/login/vendor/plugins/canvasbg/canvasbg.js"></script>

  <!-- Theme Javascript -->
  <script src="<?php echo base_url() ?>assets/login/assets/js/utility/utility.js"></script>
  <script src="<?php echo base_url() ?>assets/login/assets/js/demo/demo.js"></script>
  <script src="<?php echo base_url() ?>assets/login/assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

    $('#btn_login').click(function(){
      event.preventDefault();
      $.ajax({
          url : '<?php echo base_url() ?>login/getlogin',
          type : "POST",
          data : $('#contact').serialize(),
          dataType : 'json',
          success: function(data){
            if (data.pesan=='valid') {
             swal({
                title: "LOGIN SUKSES",
                text: "Anda Berhasil Login",
                type: "success",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
              },
              function(){
                setTimeout(function(){
                  window.location.href = '<?php echo base_url() ?>dashboard';
                }, 2000);
              });
            }else{
             swal("Login Gagal", "Username dan Password tidak valid..", "error"); 
            }
            
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>
