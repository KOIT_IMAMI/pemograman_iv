 <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url() ?>assets/unuja.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>SELAMAT DATANG</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
         <ul class="sidebar-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url() ?>Dashboard"><i class="fa fa-circle-o"></i> Dashboard</a></li>
              </ul>
            </li>
      
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url() ?>tahun_anggaran"><i class="fa fa-circle-o"></i> Tahun Anggaran</a></li>
                <li><a href="<?php echo base_url() ?>Penanggung_jawab"><i class="fa fa-circle-o"></i> Penanggung Jawab</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-bank"></i>
                <span>Master Rekening</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url() ?>struk_rekening"><i class="fa fa-circle-o"></i> Struk Rekening</a></li>
                <li><a href="<?php echo base_url() ?>kelompok_rekening"><i class="fa fa-circle-o"></i> Kelompok Rekening</a></li>
                <li><a href="<?php echo base_url() ?>jenis_rekening"><i class="fa fa-circle-o"></i> Jenis Rekening</a></li>
                <li><a href="<?php echo base_url() ?>obyek_rekening"><i class="fa fa-circle-o"></i> Obyek Rekening</a></li>
                <li><a href="<?php echo base_url() ?>rincian_rekening"><i class="fa fa-circle-o"></i> Rincian Rekening</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Transakasi Anggaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li><a href="<?php echo base_url() ?>Penyusunan_anggaran"><i class="fa fa-circle-o"></i> Penyusunan Anggran</a></li>
              </ul>
            </li>
            
          </ul>
        </section>