<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'halaman_dashboard';
		$data['anggaran_pertahun'] = $this->db->query("SELECT SUM(jumlah_penggunaan) as jml, tahun FROM v_penyusunan_anggaran GROUP BY id_tahun");
		$data['anggaran_perobyek'] = $this->db->query("SELECT SUM(jumlah_penggunaan) as jml,obyek_rekening, tahun FROM v_penyusunan_anggaran GROUP BY id_obyek_rekening, id_tahun");
		$data['tahun'] = $this->db->get('tahun');
		$data['pendapatan'] = $this->db->query("SELECT jenis, sum(jumlah_pak) as jml, tahun FROM v_penyusunan_anggaran WHERE jenis = 'D' GROUP BY id_tahun ORDER BY tahun asc limit 5");
		$data['pengeluaran'] = $this->db->query("SELECT jenis, sum(jumlah_pak) as jml, tahun FROM v_penyusunan_anggaran WHERE jenis = 'K' GROUP BY id_tahun ORDER BY tahun asc limit 5");
		$data['jenis_rekening'] = $this->db->get_where('v_jenis_rekening',array('status_jenis_rekening'=>'Y'));

		$data['jumlah_pak_aktif'] = $this->db->query("SELECT SUM(jumlah_pak) as jumlah_pak FROM v_penyusunan_anggaran WHERE id_tahun IN (SELECT id_tahun FROM tahun WHERE tahun.aktif = 'Y')")->row();
		$data['jumlah_penggunaan_aktif'] = $this->db->query("SELECT SUM(jumlah_penggunaan) as jumlah_penggunaan FROM v_penyusunan_anggaran WHERE id_tahun IN (SELECT id_tahun FROM tahun WHERE tahun.aktif = 'Y')")->row();
		// Pengeluaran Pertahun Aktif
		$data['jumlah_pengeluaran_aktif'] = $this->db->query("SELECT SUM(jumlah_pak) as jumlah_kredit FROM v_penyusunan_anggaran WHERE jenis = 'K' AND id_tahun IN (SELECT id_tahun FROM tahun WHERE tahun.aktif = 'Y')")->row();
		// Pendapatan Pertahun Aktif
		$data['jumlah_pendapatan_aktif'] = $this->db->query("SELECT SUM(jumlah_pak) as jumlah_debit FROM v_penyusunan_anggaran WHERE jenis = 'D' AND id_tahun IN (SELECT id_tahun FROM tahun WHERE tahun.aktif = 'Y')")->row();
		$this->load->view('v_template', $data);
	}
}
