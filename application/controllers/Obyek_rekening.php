<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obyek_rekening extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'obyek_rekening/v_list';
		$data['data'] = $this->M_apps->tampil_data('v_obyek_rekening');
		$this->load->view('v_template', $data);
	}	

	public function form_edit($id_obyek_rekening){
		$where = array('id_obyek_rekening'=>$id_obyek_rekening);
		$db = $this->M_apps->check_data($where,'v_obyek_rekening');

		$kode = explode(".", $db->id_obyek_rekening);
		$data['id_struk_rekening'] = $db->id_struk_rekening;
		$data['id_kelompok_rekening'] = $db->id_kelompok_rekening;
		$data['id_obyek_rekening'] = $db->id_obyek_rekening;
		$data['id_jenis_rekening'] = $db->id_jenis_rekening;
		$data['obyek_rekening'] = $db->obyek_rekening;
		$data['mode'] = 'edit';
		$data['kode_obyek_rekening'] = $kode[1];

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$where2 = array('status_kelompok_rekening'=>'Y','id_struk_rekening'=>$db->id_struk_rekening);
		$where3 = array('status_jenis_rekening'=>'Y','id_kelompok_rekening'=>$db->id_kelompok_rekening);
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where3,'jenis_rekening');
		$this->load->view('obyek_rekening/form', $data);
	}

	public function form_tambah(){
		$data['id_obyek_rekening'] = '';
		$data['id_struk_rekening'] = '';
		$data['id_jenis_rekening'] = '';
		$data['obyek_rekening'] = '';
		$data['kode_obyek_rekening'] = '';
		$data['status_obyek_rekening'] = '';
		$data['mode'] = 'tambah';
		$data['id_struk_rekening'] = '';

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$where2 = array('status_kelompok_rekening'=>'Y');
		$where3 = array('status_jenis_rekening'=>'Y');
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where3,'jenis_rekening');
		$this->load->view('obyek_rekening/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('v_obyek_rekening');
		$this->load->view('obyek_rekening/v_list',$data);
	}

	public function load_kelompok_rekening($id){
		$where = array('id_struk_rekening'=>$id);
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where,'kelompok_rekening');
		$this->load->view('obyek_rekening/load_kelompok_rekening',$data);
	}

	public function load_jenis_rekening($id){
		$where = array('id_kelompok_rekening'=>$id);
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where,'jenis_rekening');
		$this->load->view('obyek_rekening/load_jenis_rekening',$data);
	}

	public function simpan(){
		$data['obyek_rekening'] = $this->input->post('obyek_rekening');
		$data['obyek_rekening_status'] = 'N';
		$data['id_jenis_rekening'] = $this->input->post('id_jenis_rekening');
		// Cek ID Terakhir
		$where = $data['id_jenis_rekening'];
		$cek_data = $this->db->query("SELECT MAX(id_obyek_rekening) as Kolom_max FROM obyek_rekening WHERE id_jenis_rekening = '$where'")->row();
		$array_id = explode(".", $cek_data->Kolom_max);
		if (is_null($cek_data->Kolom_max)) {
			$id= "01";
		}else{
			$id = $array_id[1]+1;
			$id = "0".$id;
		}
		
		$data['id_obyek_rekening'] = $data['id_jenis_rekening'].".".$id;
		
		$this->M_apps->input_data($data,'obyek_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));

		
	}

	public function update_data(){
		$id_obyek_rekening = $this->input->post('id_obyek_rekening');
		// $data['obyek_rekening'] = $this->input->post('obyek_rekening');
		$id_struk_rekening = $this->input->post('id_struk_rekening');
		$id_kelompok_rekening = $this->input->post('id_kelompok_rekening');
		$data['id_jenis_rekening'] = $this->input->post('id_jenis_rekening');

		// PECAH ID OBYEK REKENING
		$array_id_obyek_rekening_post = explode(".", $id_obyek_rekening);

		if ($array_id_obyek_rekening_post[0].".".$array_id_obyek_rekening_post[1].".".$array_id_obyek_rekening_post[2] == $data['id_jenis_rekening']) {
			$data['id_obyek_rekening'] = $id_obyek_rekening;
			$where = array('id_obyek_rekening'=>$id_obyek_rekening);
			$this->M_apps->update_data($where,$data,'obyek_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}else{
			$where = $data['id_jenis_rekening'];
			$cek_data = $this->db->query("SELECT MAX(id_obyek_rekening) as Kolom_max FROM obyek_rekening WHERE id_jenis_rekening = '$where'")->row();
			$array_id = explode(".", $cek_data->Kolom_max);
			if (is_null($cek_data->Kolom_max)) {
				$id="01";
			}else{
				$id = $array_id[1]+1;
				$id = "0".$id;
			}
			$data['id_obyek_rekening'] = $data['id_jenis_rekening'].".".$id;
			$where = array('id_obyek_rekening'=>$id_obyek_rekening);
			$this->M_apps->update_data($where,$data,'obyek_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}
	}

	public function status_edit($id,$status){
		$id_obyek_rekening = $id;
		$data['id_obyek_rekening'] = $id;
		if ($status=='Y') {
			$data['obyek_rekening_status'] = 'N';
		}else{
			$data['obyek_rekening_status'] = 'Y';
		}

		$where = array('id_obyek_rekening'=>$id_obyek_rekening);
		$this->M_apps->update_data($where,$data,'obyek_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
