<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rincian_rekening extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'rincian_rekening/v_list';
		$data['data'] = $this->M_apps->tampil_data('v_rincian_rekening');
		$this->load->view('v_template', $data);
	}	

	public function form_edit($id_rincian_rekening){
		$where = array('id_rincian_rekening'=>$id_rincian_rekening);
		$db = $this->M_apps->check_data($where,'v_rincian_rekening');

		$kode = explode(".", $db->id_rincian_rekening);
		$data['id_struk_rekening'] = $db->id_struk_rekening;
		$data['id_kelompok_rekening'] = $db->id_kelompok_rekening;
		$data['id_rincian_rekening'] = $db->id_rincian_rekening;
		$data['id_jenis_rekening'] = $db->id_jenis_rekening;
		$data['id_obyek_rekening'] = $db->id_obyek_rekening;
		$data['rincian_rekening'] = $db->rincian_rekening;
		$data['mode'] = 'edit';
		$data['kode_rincian_rekening'] = $kode[1];

		$where = array('status_struk_rekening'=>'Y');
		$where2 = array('id_struk_rekening'=>$db->id_struk_rekening);
		$where3 = array('id_kelompok_rekening'=>$db->id_kelompok_rekening);
		$where4 = array('id_jenis_rekening'=>$db->id_jenis_rekening);
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where3,'jenis_rekening');
		$data['db_obyek_rekening'] = $this->M_apps->check_data_result($where4,'obyek_rekening');

		$this->load->view('rincian_rekening/form', $data);
	}

	public function form_tambah(){
		$data['id_rincian_rekening'] = '';
		$data['rincian_rekening'] = '';
		$data['kode_rincian_rekening'] = '';
		$data['rincian_rekening_status'] = '';
		$data['mode'] = 'tambah';
		$data['id_struk_rekening'] = '';
		$data['id_obyek_rekening'] = '';
		$data['id_rincian_rekening'] = '';
		$data['id_jenis_rekening'] = '';

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$where2 = array('status_kelompok_rekening'=>'Y');
		$where3 = array('status_jenis_rekening'=>'Y');
		$where4 = array('obyek_rekening_status'=>'Y');
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where3,'jenis_rekening');
		$data['db_obyek_rekening'] = $this->M_apps->check_data_result($where4,'obyek_rekening');
		$this->load->view('rincian_rekening/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('v_rincian_rekening');
		$this->load->view('rincian_rekening/v_list',$data);
	}

	public function load_kelompok_rekening($id){
		$where = array('id_struk_rekening'=>$id);
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where,'kelompok_rekening');
		$this->load->view('rincian_rekening/load_kelompok_rekening',$data);
	}

	public function load_jenis_rekening($id){
		$where = array('id_kelompok_rekening'=>$id);
		$data['db_jenis_rekening'] = $this->M_apps->check_data_result($where,'jenis_rekening');
		$this->load->view('rincian_rekening/load_jenis_rekening',$data);
	}

	public function load_obyek_rekening($id){
		$where = array('id_jenis_rekening'=>$id);
		$data['db_obyek_rekening'] = $this->M_apps->check_data_result($where,'obyek_rekening');
		$this->load->view('rincian_rekening/load_obyek_rekening',$data);
	}

	public function simpan(){
		$data['rincian_rekening'] = $this->input->post('rincian_rekening');
		$data['rincian_rekening_status'] = 'N';
		$data['id_obyek_rekening'] = $this->input->post('id_obyek_rekening');
		// Cek ID Terakhir
		$where = $data['id_obyek_rekening'];
		$cek_data = $this->db->query("SELECT MAX(id_rincian_rekening) as Kolom_max FROM rincian_rekening WHERE id_obyek_rekening = '$where'")->row();
		$array_id = explode(".", $cek_data->Kolom_max);
		if (is_null($cek_data->Kolom_max)) {
			$id="01";
		}else{
			$id = $array_id[1]+1;
			$id = "0".$id;
		}
		
		$data['id_rincian_rekening'] = $data['id_obyek_rekening'].".".$id;
		
		$this->M_apps->input_data($data,'rincian_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));

		
	}

	public function update_data(){
		$id_rincian_rekening = $this->input->post('id_rincian_rekening');
		$data['rincian_rekening'] = $this->input->post('rincian_rekening');
		$data['id_obyek_rekening'] = $this->input->post('id_obyek_rekening');
		// PECAH ID OBYEK REKENING
		$array_explode = explode(".", $id_rincian_rekening);

		if ($array_explode[0].".".$array_explode[1].".".$array_explode[2].".".$array_explode[3]==$data['id_obyek_rekening']) {
			$data['id_rincian_rekening'] = $this->input->post('id_rincian_rekening');
			$where = array('id_rincian_rekening'=>$id_rincian_rekening);
			$this->M_apps->update_data($where,$data,'rincian_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}else{
			$where = $data['id_obyek_rekening'];
			$cek_data = $this->db->query("SELECT MAX(id_rincian_rekening) as Kolom_max FROM rincian_rekening WHERE id_obyek_rekening = '$where'")->row();
			$array_id = explode(".", $cek_data->Kolom_max);
			if (is_null($cek_data->Kolom_max)) {
				$id="01";
			}else{
				$id = $array_id[1]+1;
				$id = "0".$id;
			}
			$data['id_rincian_rekening'] = $data['id_obyek_rekening'].".".$id;
			$where = array('id_rincian_rekening'=>$id_rincian_rekening);
			$this->M_apps->update_data($where,$data,'rincian_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}

		
	}

	public function status_edit($id,$status){
		$id_rincian_rekening = $id;
		$data['id_rincian_rekening'] = $id;
		if ($status=='Y') {
			$data['rincian_rekening_status'] = 'N';
		}else{
			$data['rincian_rekening_status'] = 'Y';
		}

		$where = array('id_rincian_rekening'=>$id_rincian_rekening);
		$this->M_apps->update_data($where,$data,'rincian_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
