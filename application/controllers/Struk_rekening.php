<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struk_rekening extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'struk_rekening/v_list';
		$data['data'] = $this->M_apps->tampil_data('struk_rekening');
		$this->load->view('v_template', $data);
	}

	public function form_edit($id_struk_rekening){
		$where = array('id_struk_rekening'=>$id_struk_rekening);
		$tb_tahun = $this->M_apps->check_data($where,'struk_rekening');
		$data['id_struk_rekening'] = $tb_tahun->id_struk_rekening;
		$data['struk_rekening'] = $tb_tahun->struk_rekening;
		$data['status_struk_rekening'] = $tb_tahun->status_struk_rekening;
		$data['mode'] = 'edit';
		$this->load->view('struk_rekening/form', $data);
	}

	public function form_tambah(){
		$data['id_struk_rekening'] = '';
		$data['struk_rekening'] = '';
		$data['status_struk_rekening'] = '';
		$data['mode'] = 'tambah';
		$this->load->view('struk_rekening/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('struk_rekening');
		$this->load->view('struk_rekening/v_list',$data);
	}

	public function simpan(){
		$data['id_struk_rekening'] = '';
		$data['struk_rekening'] = $this->input->post('struk_rekening');
		$data['status_struk_rekening'] = 'N';
		$this->M_apps->input_data($data,'struk_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function update_data(){
		$id_struk_rekening = $this->input->post('id_struk_rekening');
		$data['id_struk_rekening'] = $this->input->post('id_struk_rekening');
		$data['struk_rekening'] = $this->input->post('struk_rekening');

		$where = array('id_struk_rekening'=>$id_struk_rekening);
		$this->M_apps->update_data($where,$data,'struk_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function status_edit($id,$status){
		$id_struk_rekening = $id;
		$data['id_struk_rekening'] = $id;
		if ($status=='Y') {
			$data['status_struk_rekening'] = 'N';
		}else{
			$data['status_struk_rekening'] = 'Y';
		}

		$where = array('id_struk_rekening'=>$id_struk_rekening);
		$this->M_apps->update_data($where,$data,'struk_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
