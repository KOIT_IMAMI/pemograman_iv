<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penanggung_jawab extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'penjab/v_list';
		$data['data'] = $this->M_apps->tampil_data('penjab');
		$this->load->view('v_template', $data);
	}

	public function form_edit($id_penjab){
		$where = array('id_penjab'=>$id_penjab);
		$tb_tahun = $this->M_apps->check_data($where,'penjab');
		$data['id_penjab'] = $tb_tahun->id_penjab;
		$data['penjab'] = $tb_tahun->penjab;
		$data['aktif'] = $tb_tahun->aktif;
		$data['mode'] = 'edit';
		$this->load->view('penjab/form', $data);
	}

	public function form_tambah(){
		$data['id_penjab'] = '';
		$data['penjab'] = '';
		$data['aktif'] = '';
		$data['mode'] = 'tambah';
		$this->load->view('penjab/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('penjab');
		$this->load->view('penjab/v_list',$data);
	}

	public function simpan(){
		$data['id_penjab'] = '';
		$data['penjab'] = $this->input->post('penjab');
		$data['aktif'] = 'N';
		$this->M_apps->input_data($data,'penjab');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function update_data(){
		$id_penjab = $this->input->post('id_penjab');
		$data['id_penjab'] = $this->input->post('id_penjab');
		$data['penjab'] = $this->input->post('penjab');

		$where = array('id_penjab'=>$id_penjab);
		$this->M_apps->update_data($where,$data,'penjab');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function status_edit($id,$status){
		$id_penjab = $id;
		$data['id_penjab'] = $id;
		if ($status=='Y') {
			$data['aktif'] = 'N';
		}else{
			$data['aktif'] = 'Y';
		}

		$where = array('id_penjab'=>$id_penjab);
		$this->M_apps->update_data($where,$data,'penjab');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
