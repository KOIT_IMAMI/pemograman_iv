<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyusunan_anggaran extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$where = array('aktif' => 'Y');
		$where2 = array('status_kelompok_rekening' => 'Y');
		$where3 = array('status_struk_rekening' => 'Y');
		$data['db_tahun'] = $this->M_apps->check_data_result($where,'tahun');
		$data['struk_rekening'] = $this->M_apps->check_data_result($where3,'struk_rekening');
		$data['kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$data['obyek_rekening'] = $this->M_apps->tampil_data('obyek_rekening');
		$data['konten'] = 'Penyusunan_anggaran/v_list';
		$this->load->view('v_template', $data);
	}

	public function data($tahun)
	{
		$where = array('id_tahun' => $tahun);
		$data['tahun_anggaran']=$this->M_apps->check_data($where,'tahun');
		$data['data'] = $this->M_apps->check_data_result($where,'v_penyusunan_anggaran');
		$this->load->view('Penyusunan_anggaran/v_tabel',$data);
	}

	public function data_all()
	{
		$data['data'] = $this->M_apps->tampil_data('v_penyusunan_anggaran');
		$this->load->view('Penyusunan_anggaran/v_table_all',$data);
	}

	public function data_filter()
	{
		$where = array();
		$id_tahun = $this->input->post('id_tahun');
		$hasil_akhir = $this->input->post('hasil_akhir');
		$id_struk_rekening = $this->input->post('id_struk_rekening');
		$id_kelompok_rekening = $this->input->post('id_kelompok_rekening');
		$id_jenis_rekening = $this->input->post('id_jenis_rekening');
		$id_obyek_rekening = $this->input->post('id_obyek_rekening');

		if (!empty($id_tahun)) {
			if ($id_tahun=="semua") {
				# code...
			}else{
				$this->db->where('id_tahun',$id_tahun);
			}	
		}

		if (!empty($hasil_akhir)) {
			if ($hasil_akhir=="semua") {
				# code...
			}else if ($hasil_akhir=="-1") {
				$this->db->where('hasil_akhir <',0);
			}else if ($hasil_akhir=="1") {
				$this->db->where('hasil_akhir >',0);
			}			
			
		}

		if (!empty($id_struk_rekening)) {
			$this->db->where('id_struk_rekening',$id_struk_rekening);
		}

		if (!empty($id_kelompok_rekening)) {
			$this->db->where('id_kelompok_rekening',$id_kelompok_rekening);
		}

		if (!empty($id_jenis_rekening)) {
			$this->db->where('id_jenis_rekening',$id_jenis_rekening);
		}

		if (!empty($id_obyek_rekening)) {
			$this->db->where('id_obyek_rekening',$id_obyek_rekening);
		}
		
		// $where = array('id_tahun' => $id_tahun,'id_struk_rekening'=>$id_struk_rekening,'id_kelompok_rekening'=>$id_kelompok_rekening);
		$data['data'] = $this->db->get('v_penyusunan_anggaran')->result();
		$this->load->view('Penyusunan_anggaran/v_tabel',$data);
	}

	public function modal_add()
	{
		$data['id']="";
		$where1 = array('status_struk_rekening'=>'Y');
		$where2 = array('status_kelompok_rekening'=>'Y');
		$where3 = array('status_jenis_rekening'=>'Y');
		$where4 = array('obyek_rekening_status'=>'Y');
		$where5 = array('rincian_rekening_status'=>'Y');
		$where6 = array('aktif'=>'Y');
		$where7 = array('aktif'=>'Y');
		$data['struk_rekening'] = $this->M_apps->check_data_result($where1, 'struk_rekening');
		$data['kelompok_rekening'] = $this->M_apps->check_data_result($where2, 'kelompok_rekening');
		$data['jenis_rekening'] = $this->M_apps->check_data_result($where3, 'jenis_rekening');
		$data['obyek_rekening'] = $this->M_apps->check_data_result($where4, 'obyek_rekening');
		$data['rincian_rekening'] = $this->M_apps->check_data_result($where5, 'rincian_rekening');
		$data['tahun'] = $this->M_apps->check_data_result($where6, 'tahun');
		$data['penjab'] = $this->M_apps->check_data_result($where7, 'penjab');

		$data['id_pend_peng'] = '';
		$data['id_tahun'] = '';
		$data['id_penjab'] = '';
		$data['jenis'] = '';
		$data['uraian'] = '';
		$data['vol_penggunaan'] = '';
		$data['satuan_penggunaan'] = '';
		$data['harga_penggunaan'] = '';
		$data['tgl_realiasi'] = '';
		$data['vol_pak'] = '';
		$data['satuan_pak'] = '';
		$data['harga_pak'] = '';
		$data['at_update'] = date('Y-m-d');
		$data['id_rincian_rekening'] = '';
		$data['id_obyek_rekening'] = '';
		$data['id_kelompok_rekening'] = '';
		$data['id_jenis_rekening'] = '';
		$data['id_struk_rekening'] = '';
		$data['jumlah_penggunaan'] = '';
		$data['jumlah_pak'] = '';
		$data['mode'] = 'tambah';
        $this->load->view('Penyusunan_anggaran/v_modal', $data, FALSE);
	}

	public function modal_edit($id)
	{

		$where = array('id_pend_peng'=>$id);
		$db = $this->M_apps->check_data($where,'v_penyusunan_anggaran');
		$data['id']="";
		$where1 = array('status_struk_rekening'=>'Y');
		$where2 = array('status_kelompok_rekening'=>'Y','id_struk_rekening'=>$db->id_struk_rekening);
		$where3 = array('status_jenis_rekening'=>'Y','id_kelompok_rekening'=>$db->id_kelompok_rekening);
		$where4 = array('obyek_rekening_status'=>'Y','id_jenis_rekening'=>$db->id_jenis_rekening);
		$where5 = array('rincian_rekening_status'=>'Y','id_obyek_rekening'=>$db->id_obyek_rekening);
		$where6 = array('aktif'=>'Y');
		$where7 = array('aktif'=>'Y');
		$data['struk_rekening'] = $this->M_apps->check_data_result($where1, 'struk_rekening');
		$data['kelompok_rekening'] = $this->M_apps->check_data_result($where2, 'kelompok_rekening');
		$data['jenis_rekening'] = $this->M_apps->check_data_result($where3, 'jenis_rekening');
		$data['obyek_rekening'] = $this->M_apps->check_data_result($where4, 'obyek_rekening');
		$data['rincian_rekening'] = $this->M_apps->check_data_result($where5, 'rincian_rekening');
		$data['tahun'] = $this->M_apps->check_data_result($where6, 'tahun');
		$data['penjab'] = $this->M_apps->check_data_result($where7, 'penjab');

		$data['id_pend_peng'] = $db->id_pend_peng;
		$data['id_tahun'] = $db->id_tahun;
		$data['id_penjab'] = $db->id_penjab;
		$data['jenis'] = $db->jenis;
		$data['uraian'] = $db->uraian;
		$data['vol_penggunaan'] = $db->vol_penggunaan;
		$data['satuan_penggunaan'] = $db->satuan_penggunaan;
		$data['harga_penggunaan'] = $db->harga_penggunaan;
		$data['tgl_realiasi'] = $db->tgl_realiasi;
		$data['vol_pak'] = $db->vol_pak;
		$data['satuan_pak'] = $db->satuan_pak;
		$data['harga_pak'] = $db->harga_pak;
		$data['at_update'] = date('Y-m-d');
		$data['id_rincian_rekening'] = $db->id_rincian_rekening;
		$data['id_obyek_rekening'] = $db->id_obyek_rekening;
		$data['id_kelompok_rekening'] = $db->id_kelompok_rekening;
		$data['id_jenis_rekening'] = $db->id_jenis_rekening;
		$data['id_struk_rekening'] = $db->id_struk_rekening;
		$data['jumlah_penggunaan'] = $db->jumlah_penggunaan;
		$data['jumlah_pak'] = $db->jumlah_pak;
		$data['mode'] = 'update';
        $this->load->view('Penyusunan_anggaran/v_modal', $data, FALSE);
	}

	public function load_kelompok_rekening($id){
		$where = array('id_struk_rekening'=>$id);
		$data['kelompok_rekening'] = $this->M_apps->check_data_result($where,'kelompok_rekening');
		$this->load->view('Penyusunan_anggaran/load_kelompok_rekening',$data);
	}

	public function load_jenis_rekening($id){
		$where = array('id_kelompok_rekening'=>$id);
		$data['jenis_rekening'] = $this->M_apps->check_data_result($where,'jenis_rekening');
		$this->load->view('Penyusunan_anggaran/load_jenis_rekening',$data);
	}

	public function load_obyek_rekening($id){
		$where = array('id_jenis_rekening'=>$id);
		$data['obyek_rekening'] = $this->M_apps->check_data_result($where,'obyek_rekening');
		$this->load->view('Penyusunan_anggaran/load_obyek_rekening',$data);
	}

	public function load_rincian_rekening($id){
		$where = array('id_obyek_rekening'=>$id);
		$data['rincian_rekening'] = $this->M_apps->check_data_result($where,'rincian_rekening');
		$this->load->view('Penyusunan_anggaran/load_rincian_rekening',$data);
	}

	public function simpan(){
		$data['id_pend_peng'] = '';
		$data['id_tahun'] = $this->input->post('id_tahun');
		$data['id_penjab'] = $this->input->post('id_penjab');
		$data['jenis'] = $this->input->post('jenis');
		$data['uraian'] = $this->input->post('uraian');
		$data['vol_penggunaan'] = $this->input->post('vol_penggunaan');
		$data['satuan_penggunaan'] = $this->input->post('satuan_penggunaan');
		$data['harga_penggunaan'] = $this->input->post('harga_penggunaan');
		$data['tgl_realiasi'] = $this->input->post('tgl_realiasi');
		$data['vol_pak'] = $this->input->post('vol_pak');
		$data['satuan_pak'] = $this->input->post('satuan_pak');
		$data['harga_pak'] = $this->input->post('harga_pak');
		$data['at_insert'] = date('Y-m-d');
		$data['id_rincian_rekening'] = $this->input->post('id_rincian_rekening');
		$this->M_apps->input_data($data,'pend_peng');
		echo json_encode(array("status"=>true,'pesan'=>'simpan','tahun'=>$this->input->post('id_tahun')));
	}


	public function update_data(){
		$data['id_pend_peng'] = $this->input->post('id_pend_peng');
		$data['id_tahun'] = $this->input->post('id_tahun');
		$data['id_penjab'] = $this->input->post('id_penjab');
		$data['jenis'] = $this->input->post('jenis');
		$data['uraian'] = $this->input->post('uraian');
		$data['vol_penggunaan'] = $this->input->post('vol_penggunaan');
		$data['satuan_penggunaan'] = $this->input->post('satuan_penggunaan');
		$data['harga_penggunaan'] = $this->input->post('harga_penggunaan');
		$data['tgl_realiasi'] = $this->input->post('tgl_realiasi');
		$data['vol_pak'] = $this->input->post('vol_pak');
		$data['satuan_pak'] = $this->input->post('satuan_pak');
		$data['harga_pak'] = $this->input->post('harga_pak');
		$data['at_update'] = date('Y-m-d');
		$data['id_rincian_rekening'] = $this->input->post('id_rincian_rekening');

		$where = array('id_pend_peng'=>$this->input->post('id_pend_peng'));
		$this->M_apps->update_data($where,$data,'pend_peng');
		echo json_encode(array("status"=>true,'pesan'=>'simpan','tahun'=>$this->input->post('id_tahun')));
	}

	public function hapus($id){
		$where = array('id_pend_peng'=>$id);
		$db = $this->M_apps->check_data($where,'v_penyusunan_anggaran');
		$this->M_apps->delete_data($where,'pend_peng');
		echo json_encode(array("status"=>true,'pesan'=>'hapus','tahun'=>$db->id_tahun));
	}

	public function load_field_kelompok($id){
		$where = array('id_struk_rekening'=>$id);
		$data['kelompok_rekening'] = $this->M_apps->check_data_result($where,'kelompok_rekening');
		$this->load->view('Penyusunan_anggaran/load_data_rekening/load_kelompok_rekening', $data);
	}

	public function load_field_jenis($id){
		$where = array('id_kelompok_rekening'=>$id);
		$data['jenis_rekening'] = $this->M_apps->check_data_result($where,'jenis_rekening');
		$this->load->view('Penyusunan_anggaran/load_data_rekening/load_jenis_rekening', $data);
	}

	public function load_field_obyek($id){
		$where = array('id_jenis_rekening'=>$id);
		$data['obyek_rekening'] = $this->M_apps->check_data_result($where,'obyek_rekening');
		$this->load->view('Penyusunan_anggaran/load_data_rekening/load_obyek_rekening', $data);
	}
}
