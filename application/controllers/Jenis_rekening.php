<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_rekening extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'jenis_rekening/v_list';
		$data['data'] = $this->M_apps->tampil_data('v_jenis_rekening');
		$this->load->view('v_template', $data);
	}	

	public function form_edit($id_jenis_rekening){
		$where = array('id_jenis_rekening'=>$id_jenis_rekening);
		$db = $this->M_apps->check_data($where,'v_jenis_rekening');

		$kode = explode(".", $db->id_jenis_rekening);
		$data['id_struk_rekening'] = $db->id_struk_rekening;
		$data['id_kelompok_rekening'] = $db->id_kelompok_rekening;
		$data['id_jenis_rekening'] = $db->id_jenis_rekening;
		$data['jenis_rekening'] = $db->jenis_rekening;
		$data['status_jenis_rekening'] = $db->status_jenis_rekening;
		$data['mode'] = 'edit';
		$data['kode_jenis_rekening'] = $kode[1];

		$where = array('status_struk_rekening'=>'Y');
		$where2 = array('id_struk_rekening'=>$db->id_struk_rekening);
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$this->load->view('jenis_rekening/form', $data);
	}

	public function form_tambah(){
		$data['id_jenis_rekening'] = '';
		$data['jenis_rekening'] = '';
		$data['kode_jenis_rekening'] = '';
		$data['status_jenis_rekening'] = '';
		$data['mode'] = 'tambah';
		$data['id_struk_rekening'] = '';

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$where2 = array('status_kelompok_rekening'=>'Y');
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where2,'kelompok_rekening');
		$this->load->view('jenis_rekening/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('v_jenis_rekening');
		$this->load->view('jenis_rekening/v_list',$data);
	}

	public function load_kelompok_rekening($id){
		$where = array('id_struk_rekening'=>$id);
		$data['db_kelompok_rekening'] = $this->M_apps->check_data_result($where,'kelompok_rekening');
		$this->load->view('jenis_rekening/load_kelompok_rekening',$data);
	}

	public function simpan(){
		$data['jenis_rekening'] = $this->input->post('jenis_rekening');
		$data['status_jenis_rekening'] = 'N';
		$data['id_kelompok_rekening'] = $this->input->post('id_kelompok_rekening');
		// Cek ID Terakhir
		$where = $data['id_kelompok_rekening'];
		$cek_data = $this->db->query("SELECT MAX(id_jenis_rekening) as Kolom_max FROM jenis_rekening WHERE id_kelompok_rekening = '$where'")->row();
		$array_id = explode(".", $cek_data->Kolom_max);
		if (is_null($cek_data->Kolom_max)) {
			$id=1;
		}else{
			$id = $array_id[1]+1;
		}
		
		$data['id_jenis_rekening'] = $data['id_kelompok_rekening'].".".$id;
		
		$this->M_apps->input_data($data,'jenis_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));

		
	}

	public function update_data(){
		$id_jenis_rekening = $this->input->post('id_jenis_rekening');
		$data['jenis_rekening'] = $this->input->post('jenis_rekening');
		$id_struk_rekening = $this->input->post('id_struk_rekening');
		$data['id_kelompok_rekening'] = $this->input->post('id_kelompok_rekening');

		if ($id_jenis_rekening == $id_struk_rekening.".".$data['id_kelompok_rekening']) {
			$data['id_jenis_rekening'] = $id_jenis_rekening;
			$where = array('id_jenis_rekening'=>$id_jenis_rekening);
			$this->M_apps->update_data($where,$data,'jenis_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}else{
			$where = $data['id_kelompok_rekening'];
			$cek_data = $this->db->query("SELECT MAX(id_jenis_rekening) as Kolom_max FROM jenis_rekening WHERE id_kelompok_rekening = '$where'")->row();
			$array_id = explode(".", $cek_data->Kolom_max);
			if (is_null($cek_data->Kolom_max)) {
				$id=1;
			}else{
				$id = $array_id[1]+1;
			}
			$data['id_jenis_rekening'] = $data['id_kelompok_rekening'].".".$id;
			$where = array('id_jenis_rekening'=>$id_jenis_rekening);
			$this->M_apps->update_data($where,$data,'jenis_rekening');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}
		
	}

	public function status_edit($id,$status){
		$id_jenis_rekening = $id;
		$data['id_jenis_rekening'] = $id;
		if ($status=='Y') {
			$data['status_jenis_rekening'] = 'N';
		}else{
			$data['status_jenis_rekening'] = 'Y';
		}

		$where = array('id_jenis_rekening'=>$id_jenis_rekening);
		$this->M_apps->update_data($where,$data,'jenis_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
