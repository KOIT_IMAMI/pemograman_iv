<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_rekening extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'kelompok_rekening/v_list';
		$data['data'] = $this->M_apps->tampil_data('kelompok_rekening');
		$this->load->view('v_template', $data);
	}	

	public function form_edit($id_kelompok_rekening){
		$where = array('id_kelompok_rekening'=>$id_kelompok_rekening);
		$db = $this->M_apps->check_data($where,'kelompok_rekening');

		$kode = explode(".", $db->id_kelompok_rekening);
		$data['id_struk_rekening'] = $db->id_struk_rekening;
		$data['id_kelompok_rekening'] = $db->id_kelompok_rekening;
		$data['kelompok_rekening'] = $db->kelompok_rekening;
		$data['status_kelompok_rekening'] = $db->status_kelompok_rekening;
		$data['mode'] = 'edit';
		$data['kode_kelompok_rekening'] = $kode[1];

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$this->load->view('kelompok_rekening/form', $data);
	}

	public function form_tambah(){
		$data['id_kelompok_rekening'] = '';
		$data['kelompok_rekening'] = '';
		$data['kode_kelompok_rekening'] = '';
		$data['status_kelompok_rekening'] = '';
		$data['mode'] = 'tambah';
		$data['id_struk_rekening'] = '';

		$where = array('status_struk_rekening'=>'Y');
		$data['db_struk'] = $this->M_apps->check_data_result($where,'struk_rekening');
		$this->load->view('kelompok_rekening/form', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('kelompok_rekening');
		$this->load->view('kelompok_rekening/v_list',$data);
	}

	public function simpan(){

		$data['kelompok_rekening'] = $this->input->post('kelompok_rekening');
		$data['status_kelompok_rekening'] = 'N';
		$data['id_struk_rekening'] = $this->input->post('id_struk_rekening');
		// Cek ID Terakhir
		$where = $data['id_struk_rekening'];
		$cek_data = $this->db->query("SELECT MAX(id_kelompok_rekening) as Kolom_max FROM kelompok_rekening WHERE id_struk_rekening = '$where'")->row();
		$array_id = explode(".", $cek_data->Kolom_max);
		if (is_null($cek_data->Kolom_max)) {
			$id=1;
		}else{
			$id = $array_id[1]+1;
		}
		
		$data['id_kelompok_rekening'] = $data['id_struk_rekening'].".".$id;
		
		$this->M_apps->input_data($data,'kelompok_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));

		
	}

	public function update_data(){
		$id_kelompok_rekening = $this->input->post('id_kelompok_rekening');
		
		$data['kelompok_rekening'] = $this->input->post('kelompok_rekening');
		$data['id_struk_rekening'] = $this->input->post('id_struk_rekening');

		$array_explode = explode(".", $id_kelompok_rekening);

		if ($array_explode[0]==$data['id_struk_rekening']) {
			$data['id_kelompok_rekening'] = $this->input->post('id_kelompok_rekening');
		}else{
			$where = $data['id_struk_rekening'];
			$cek_data = $this->db->query("SELECT MAX(id_kelompok_rekening) as Kolom_max FROM kelompok_rekening WHERE id_struk_rekening = '$where'")->row();
			$array_id = explode(".", $cek_data->Kolom_max);
			if (is_null($cek_data->Kolom_max)) {
				$id=1;
			}else{
				$id = $array_id[1]+1;
			}
			$data['id_kelompok_rekening'] = $data['id_struk_rekening'].".".$id;
		}

		$where = array('id_kelompok_rekening'=>$id_kelompok_rekening);
		$this->M_apps->update_data($where,$data,'kelompok_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function status_edit($id,$status){
		$id_kelompok_rekening = $id;
		$data['id_kelompok_rekening'] = $id;
		if ($status=='Y') {
			$data['status_kelompok_rekening'] = 'N';
		}else{
			$data['status_kelompok_rekening'] = 'Y';
		}

		$where = array('id_kelompok_rekening'=>$id_kelompok_rekening);
		$this->M_apps->update_data($where,$data,'kelompok_rekening');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}
}
