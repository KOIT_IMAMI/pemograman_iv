<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun_anggaran extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('M_apps');
		// if ($this->session->userdata('SISTEM')!='SIBEKA') {
		// 	redirect('sibeka/login_sibeka','refresh');
		// }
	}

	public function index()
	{
		$data['konten'] = 'Tahun_anggaran/v_list_tahun_anggaran';
		$data['data'] = $this->M_apps->tampil_data('tahun');
		$this->load->view('v_template', $data);
	}

	public function form_edit($id_tahun){
		$where = array('id_tahun'=>$id_tahun);
		$tb_tahun = $this->M_apps->check_data($where,'tahun');
		$data['id_tahun'] = $tb_tahun->id_tahun;
		$data['tahun'] = $tb_tahun->tahun;
		$data['aktif'] = $tb_tahun->aktif;
		$data['keterangan'] = $tb_tahun->keterangan;
		$data['mode'] = 'edit';
		$this->load->view('Tahun_anggaran/form_tahun_anggaran', $data);
	}

	public function form_tambah(){
		$data['id_tahun'] = '';
		$data['tahun'] = '';
		$data['aktif'] = '';
		$data['keterangan'] = '';
		$data['mode'] = 'tambah';
		$this->load->view('Tahun_anggaran/form_tahun_anggaran', $data);
	}

	public function data()
	{
		$data['data'] = $this->M_apps->tampil_data('tahun');
		$this->load->view('Tahun_anggaran/v_list_tahun_anggaran',$data);
	}

	public function simpan(){
		$data['id_tahun'] = '';
		$data['tahun'] = $this->input->post('tahun');
		$data['aktif'] = 'N';
		$data['keterangan'] = $this->input->post('keterangan');
		$this->M_apps->input_data($data,'tahun');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function update_data(){
		$id_tahun = $this->input->post('id_tahun');
		$data['id_tahun'] = $this->input->post('id_tahun');
		$data['tahun'] = $this->input->post('tahun');
		$data['keterangan'] = $this->input->post('keterangan');

		$where = array('id_tahun'=>$id_tahun);
		$this->M_apps->update_data($where,$data,'tahun');
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function status_edit($id,$status){
		$id_tahun = $id;
		$data['id_tahun'] = $id;
		if ($status=='Y') {
			$data['aktif'] = 'N';
		}else{
			$data['aktif'] = 'Y';
		}

		$where = array('aktif'=>'Y');
		$cek = $this->M_apps->check_data_num_rows($where,'tahun');

		if ($cek->num_rows()<=1) {
			if ($status=='N') {
				$where = array('id_tahun'=>$id_tahun);
				$this->M_apps->update_data($where,$data,'tahun');
				echo json_encode(array("status"=>true,'pesan'=>'simpan'));
			}else{
				echo json_encode(array("status"=>true,'pesan'=>'warning'));
			}
		}else{
			$where = array('id_tahun'=>$id_tahun);
			$this->M_apps->update_data($where,$data,'tahun');
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
		}

		
	}
}
