<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
	
	public function index()
	{
		$this->load->view('halaman_login');
	}

	public function getlogin()
	{
		
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->load->model('m_login');
		$get_login=$this->m_login->login($u,$p);
		if ($get_login>0) {
			echo json_encode(array("status"=>true,'pesan'=>'valid'));
		}else{
			echo json_encode(array("status"=>true,'pesan'=>'gagal'));
		}
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}